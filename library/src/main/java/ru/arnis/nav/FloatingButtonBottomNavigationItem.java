package ru.arnis.nav;

import android.animation.Animator;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arnis on 23/12/2016.
 */

class FloatingButtonBottomNavigationItem {
    private static ItemManager itemManager = new FloatingButtonBottomNavigationItem.ItemManager();
    static FloatingButtonBottomNavigationItem getItem(int index){
        return itemManager.items.get(index);
    }
    private static final int ITEM_HEIGHT = 80;
    static int getItemHeight(float density) {
        return (int) (ITEM_HEIGHT*density);
    }

    private TextView textView;
    private ImageView imageView;
    private ImageView circle;
    private boolean active;

    private boolean isActive() {
        return active;
    }
    private void setActive(boolean active) {
        this.active = active;
    }
    void setTitle(String text) {
        textView.setText(text);
    }

    void clear(){
        textView = null;
        imageView = null;
        circle = null;
    }

    FloatingButtonBottomNavigationItem(View view) {
        this.imageView = (ImageView) view.findViewById(R.id.dash);
        this.textView = (TextView) view.findViewById(R.id.title);
        circle = (ImageView) view.findViewById(R.id.selection);
        itemManager.newItem(this);
    }

    void animateItemTransition(final boolean isShowing, long delay){
        if (!isShowing){
            imageView.animate().setStartDelay(delay+100).alpha(0f).setDuration(300);
            textView.animate().setStartDelay(delay).alpha(1f).scaleX(1f).scaleY(1f).setDuration(500).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (isActive())
                        selection(!isShowing,false);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            imageView.animate().setStartDelay(delay).alpha(1f).setDuration(350).start();
            textView.animate().setStartDelay(delay).alpha(0f).scaleX(0f).scaleY(0f).setDuration(400).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (isActive())
                        selection(!isShowing,false);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
        }
    }

    private void selection(boolean select, final boolean reverseAfterComplete){
        if (select){
            circle.setScaleX(0);
            circle.setScaleY(0);
            circle.setVisibility(View.VISIBLE);
            circle.animate().scaleY(1f).scaleX(1f).setInterpolator(new OvershootInterpolator()).setDuration(400).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (reverseAfterComplete) {
                        hideSelection();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            textView.setTypeface(textView.getTypeface(),Typeface.BOLD);
            textView.setTextColor(Color.WHITE);
        } else {
            hideSelection();
        }
    }

    private void hideSelection(){
        circle.animate().scaleY(0f).scaleX(0f).setInterpolator(new OvershootInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                circle.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).setDuration(50);
        textView.setTypeface(textView.getTypeface(),Typeface.NORMAL);
        textView.setTextColor(Color.rgb(224,224,224));// TODO: 07/01/2017
    }

    static void setSelection(int index){
        itemManager.setSelection(index);
    }
    static void clearAll(){
        itemManager.clear();
    }

    private static class ItemManager{
        private List<FloatingButtonBottomNavigationItem> items;
        private FloatingButtonBottomNavigationItem activeItem;
        private FloatingButtonBottomNavigationItem lastItem;

        ItemManager() {
            items = new ArrayList<>();
        }

        void clear(){
            for (FloatingButtonBottomNavigationItem item:items){
                item.clear();
            }
            items.clear();
        }

        void newItem(FloatingButtonBottomNavigationItem item){
            if (items.size()==0){
                activeItem = item;
                lastItem = item;
                item.setActive(true);
            }
            items.add(item);
        }

        void setSelection(int index){
            activeItem = items.get(index);
            activeItem.setActive(true);
            activeItem.selection(true,true);
            boolean isNewItemClick = lastItem != null && lastItem != activeItem;
            if (lastItem!=null&&isNewItemClick){
                lastItem.selection(false,false);
                lastItem.setActive(false);
            }
            lastItem = activeItem;
        }


    }
}
