package ru.arnis.nav;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import io.codetail.animation.ViewAnimationUtils;


/**
 * Created by arnis on 23/12/2016.
 */

public class FloatingButtonBottomNavigation extends RelativeLayout{
    private LinearLayout itemsList;
    private View reveal;
    private FloatingButton floatingButton;
    private ImageView dim;
    private GestureDetector gd;
    private boolean isShowing;
    private boolean animating;

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public boolean isShowing() {
        return isShowing;
    }
    public boolean isAnimating() {
        return animating;
    }
    public void setAnimating(boolean animating) {
        this.animating = animating;
    }

    public FloatingButtonBottomNavigation(Context context) {
        super(context);
    }

    public FloatingButtonBottomNavigation(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloatingButtonBottomNavigation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init(final String... itemTitles){

        gd = new GestureDetector(getContext(),new GestureListener());

        int navHeight = FloatingButtonBottomNavigationItem.getItemHeight(getResources().getDisplayMetrics().density)*itemTitles.length;

        LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, navHeight);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);

        ScrollView itemsListWrapping = new ScrollView(getContext());
        itemsListWrapping.setLayoutParams(params);
        itemsList = new LinearLayout(getContext());
        itemsListWrapping.addView(itemsList);
        itemsList.setOrientation(LinearLayout.VERTICAL);
        itemsList.setVisibility(INVISIBLE);
        for (String title:itemTitles){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_item,null);
            new FloatingButtonBottomNavigationItem(view).setTitle(title);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int indexOfView = itemsList.indexOfChild(view);

                    FloatingButtonBottomNavigationItem.setSelection(indexOfView);
                    hideNav(260);

                    if (onItemClickListener!=null)
                        onItemClickListener.onItemClick(indexOfView);
                }
            });
            itemsList.addView(view);
        }

        View revealBack = LayoutInflater.from(getContext()).inflate(R.layout.reveal_background,null);
        revealBack.setLayoutParams(params);
        reveal = revealBack.findViewById(R.id.reveal);

        floatingButton = new FloatingButton((RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.custom_button,null));
        params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
        floatingButton.getParentView().setLayoutParams(params);

        dim = new ImageView(getContext());
        dim.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dim.setAlpha(0f);
        dim.setBackgroundColor(getResources().getColor(R.color.dark));

        this.addView(dim);
        this.addView(revealBack);
        this.addView(itemsListWrapping);
        this.addView(floatingButton.getParentView());
    }

    private void showNav(long delay){
        if (!isShowing){
            setAnimating(true);
            reveal(delay);
            animateList(delay);
            dissolveButton(delay);
            dim(delay);
        }
    }
    private void hideNav(long delay){
        if (isShowing){
            setAnimating(true);
            reveal(delay);
            animateList(delay);
            expandButton(delay);
            dim(delay);
        }
    }
    public void forceHide(){
        hideNav(0);
    }

    private void dissolveButton(long delay){
        floatingButton.dissolve();
    }
    private void expandButton(long delay){
        floatingButton.appear(delay);
    }
    public void hideButton(){
        floatingButton.hide();
    }
    public void showButton(){
        floatingButton.show();
    }



    private void reveal(long delay){
        floatingButton.setButtonCenter();

        int cx = (reveal.getLeft() + reveal.getRight()) / 2;
        int cy = floatingButton.getButtonCenter();

        int dx = Math.max(cx, this.getWidth() - cx);
        int dy = Math.max(cy, this.getHeight() - cy);
        final float finalRadius = (float) Math.hypot(dx, dy);

        Animator animator;
        if (!isShowing){
            animator = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, floatingButton.getRadius(), finalRadius);
        }
        else{
            animator = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, finalRadius, 0);
        }

        animator.setInterpolator(new DecelerateInterpolator());
        animator.setDuration(350);
        animator.setStartDelay(delay);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                reveal.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setAnimating(false);
                if (isShowing){
                    reveal.setVisibility(INVISIBLE);
                    itemsList.setVisibility(INVISIBLE);
                }

                isShowing = !isShowing;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    private void dim(long delay){
        if (!isShowing)
            dim.animate().setStartDelay(delay).alpha(.7f).setDuration(300).start();
        else dim.animate().setStartDelay(delay).alpha(0).setDuration(300).start();
    }

    private void animateList(long delay){
        itemsList.setVisibility(VISIBLE);
        for (int i = 0; i < itemsList.getChildCount(); i++) {
            FloatingButtonBottomNavigationItem.getItem(i).animateItemTransition(isShowing,delay);
            View child = itemsList.getChildAt(i);
            animateItem(child,i,delay);
        }
    }
    private void animateItem(View view, int iter, long delay){
        PropertyValuesHolder pvhY;
        if (!isShowing){
            pvhY = PropertyValuesHolder.ofFloat(Y,floatingButton.getAdjustedCenter(),iter*FloatingButtonBottomNavigationItem.getItemHeight(getResources().getDisplayMetrics().density));
        } else {
            pvhY = PropertyValuesHolder.ofFloat(Y,iter*FloatingButtonBottomNavigationItem.getItemHeight(getResources().getDisplayMetrics().density),floatingButton.getAdjustedCenter());
        }

        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view,pvhY);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.setStartDelay(delay);
        animator.setDuration(350);
        animator.start();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        gd.onTouchEvent(ev);
        return isAnimating();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        FloatingButtonBottomNavigationItem.clearAll();
    }

    private class FloatingButton {
        private RelativeLayout parentView;
        private View background;
        private View icon;
        private View ripple;
        private float buttonY;
        private int buttonCenter;

        public FloatingButton(RelativeLayout parentView) {
            this.parentView = parentView;
            background = parentView.findViewById(R.id.background);
            icon = parentView.findViewById(R.id.icon);
            ripple = parentView.findViewById(R.id.click);
            ripple.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showNav(0);
                }
            });
        }

        RelativeLayout getParentView(){
            return parentView;
        }

        void dissolve(){
            background.setVisibility(INVISIBLE);
            icon.setVisibility(INVISIBLE);
            parentView.postDelayed(new Runnable() {
                @Override
                public void run() {
                   parentView.setVisibility(INVISIBLE);
                }
            },500);
        }
        void appear(long delay){
            parentView.setScaleY(0f);
            parentView.setScaleX(0f);
            parentView.animate().setStartDelay(delay+200).scaleX(1f).scaleY(1f).setInterpolator(new OvershootInterpolator()).setDuration(400).start();
            background.setVisibility(VISIBLE);
            icon.setVisibility(VISIBLE);
            parentView.setVisibility(VISIBLE);
        }
        void hide(){
            setButtonY(parentView.getY());
            parentView.animate().y(dim.getHeight()).start();
        }
        void show(){
            parentView.animate().y(buttonY).start();
        }
        void setButtonCenter(){
            if (buttonCenter==0)
                buttonCenter = (int) ((parentView.getY()+(parentView.getHeight()/2)-(dim.getHeight()-reveal.getHeight())));
        }

        int getButtonCenter() {
            return buttonCenter;
        }

        int getRadius(){
            return parentView.getHeight()/2;
        }

        int getAdjustedCenter(){
            return getButtonCenter()-getRadius();
        }

        private void setButtonY(float buttonY) {
            if (this.buttonY==0)
                this.buttonY = buttonY;
        }

    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float yDif = e2.getY()-e1.getY();
            float xDif = e2.getX()-e1.getX();

            if (Math.abs(xDif)<yDif&&yDif>0){
                hideNav(0);
            }
            return false;
        }
    }
}
