package ru.arnis.movies;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.arnis.movies.assistant.Assistant;
import ru.arnis.movies.components.MyScroller;
import ru.arnis.movies.model.Comment;
import ru.arnis.movies.model.Movie;
import ru.arnis.movies.model.UserInfo;
import ru.nn2.components.OnCompleteListener;

import static butterknife.ButterKnife.findById;
import static ru.arnis.movies.MainActivity.movies;

public class MoviePage extends AppCompatActivity {
    @BindView(R.id.background)
    ImageView background;
    @BindView(R.id.scroller)
    MyScroller scroller;
    @BindView(R.id.card_movie)
    RelativeLayout cardLayout;
    @BindView(R.id.cast_scroller)
    LinearLayout castScroller;
    @BindView(R.id.genre_scroller)
    LinearLayout genreScroller;
    @BindView(R.id.bubble_infobox)
    FlexboxLayout infobox;
    @BindView(R.id.movie_description)
    TextView description;
    @BindView(R.id.activity_movie_page)
    RelativeLayout parent;
    private LinearLayout circleTouchArea;
    @BindView(R.id.circle_director)
    RelativeLayout cirlceDirector;
    @BindView(R.id.cast_scrollview)
    HorizontalScrollView castScrollview;
    @BindView(R.id.comment_feed)
    LinearLayout commentFeed;
    @BindView(R.id.overlay)
    ImageView overlay;
    private View commentLayout;

    @BindColor(R.color.boevik) int boevik;
    @BindColor(R.color.voennyi) int voennyi;
    @BindColor(R.color.drama) int drama;
    @BindColor(R.color.komediya) int komediya;
    @BindColor(R.color.kriminal) int kriminal;
    @BindColor(R.color.melodrama) int melodrama;
    @BindColor(R.color.multfilm) int multfilm;
    @BindColor(R.color.priklucheniya) int priklucheniya;
    @BindColor(R.color.triller) int triller;
    @BindColor(R.color.uzhasy) int uzhasy;
    @BindColor(R.color.fantastika) int fantastika;

    @BindDrawable(R.drawable.ic_budget)
    Drawable budget;
    @BindDrawable(R.drawable.ic_country)
    Drawable country;
    @BindDrawable(R.drawable.ic_date)
    Drawable date;
    @BindDrawable(R.drawable.ic_duration)
    Drawable duration;
    @BindDrawable(R.drawable.ic_rating)
    Drawable rating;



    private TextView rusTitle;
    private TextView engTitle;
    private Movie movie;
    private CardView card;
    private Animation animation;
    private GestureDetector gd;
    private Comment respondComment;

    private int circleTouchAreaWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_page);
        ButterKnife.bind(this);
        rusTitle = findById(cardLayout,R.id.main_rus_title);
        engTitle = findById(cardLayout,R.id.main_eng_title);
        card = findById(cardLayout,R.id.info_card);

        animation = new Animation();
//        gd = new GestureDetector(this,new GestureListener());
        scroller.setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        animation.setLayoutTransitionAnimation(commentFeed);

        setUpScroller();

        loadMovie();
        applyMovieToLayout();

        setUpCircles();
        initComment(LayoutInflater.from(this));
    }

    private void setUpCircles() {
        circleTouchArea = findById(cardLayout,R.id.circle_touch_area);

        final View circleUser = findById(cardLayout,R.id.circle_user);
        final View circleNN = findById(cardLayout,R.id.circle_nn);

        final ImageView circleUserBack = findById(cardLayout,R.id.circle_user_background);
        final ImageView circleNNBack = findById(cardLayout,R.id.circle_nn_background);

        circleTouchArea.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                circleTouchAreaWidth = circleTouchArea.getWidth()-circleUser.getWidth();
                animation.revealCircles(circleUser,circleNN,circleUserBack,circleNNBack,movie,circleTouchAreaWidth);
                if (Build.VERSION.SDK_INT < 16) {
                    circleTouchArea.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    circleTouchArea.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });


        circleTouchArea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();
//                Log.d("happy", "onTouch: "+ Integer.toString(action));
                if (action == MotionEvent.ACTION_DOWN){
                    disableScrolling();
                    animation.shrink(circleNN);
                    animation.setCircle(circleUser,circleUserBack,(int) motionEvent.getX());
                } else if (action == MotionEvent.ACTION_UP|| action == MotionEvent.ACTION_CANCEL){
                    animation.restore(circleNN);
                    float ratingX = animation.snapBackCircle();
                    enableScrolling();
                    movie.setUserRating(ratingX/(circleTouchAreaWidth));
                    Snackbar.make(parent,"Успех: " + Integer.toString(Assistant.checkPredcitionSuccess(movie)),Snackbar.LENGTH_SHORT).show();
//                    Log.d("happy", "movie rating "+Float.toString(movie.getUserRating()*100));
//                    Log.d("happy", "success "+ Integer.toString(Assistant.checkPredcitionSuccess(movie)));
                } else {
                    animation.changeCircleState(motionEvent);
                }

                return true;
            }
        });
    }

    private void setUpScroller(){
        scroller.setSmoothScrollingEnabled(true);
        scroller.setVerticalScrollBarEnabled(false);
//        scrollerChilds = findById(scroller,R.id.scroller_children);
        castScrollview.setSmoothScrollingEnabled(true);
    }
    private void disableScrolling(){
        scroller.setEnableScrolling(false);
    }
    private void enableScrolling(){
        scroller.setEnableScrolling(true);
    }

    private void applyMovieToLayout() {
        Glide.with(this).load(movie.getPosterURL()).centerCrop().into(background);
        rusTitle.setText(movie.getTitleRUS());
        engTitle.setText(movie.getTitleENG());
        populateCast(castScroller);
        populateGenre(movie.getGenre(),genreScroller);
        populateInfobox();
        description.setText(movie.getDescription());
    }

    private void populateInfobox() {
        LayoutInflater inflater = LayoutInflater.from(this);

        View view = inflater.inflate(R.layout.bubble_with_icon,null);
        ImageView icon = findById(view,R.id.bubble_icon);
        TextView text = findById(view,R.id.bubble_text);
        icon.setImageDrawable(country);
        text.setText(movie.getCountries().get(0));
        infobox.addView(view);

        view = inflater.inflate(R.layout.bubble_with_icon,null);
        icon = findById(view,R.id.bubble_icon);
        text = findById(view,R.id.bubble_text);
        icon.setImageDrawable(duration);
        text.setText(movie.getDuration());
        infobox.addView(view);

        view = inflater.inflate(R.layout.bubble_with_icon,null);
        icon = findById(view,R.id.bubble_icon);
        text = findById(view,R.id.bubble_text);
        icon.setImageDrawable(budget);
        text.setText(movie.getBudget());
        infobox.addView(view);

        view = inflater.inflate(R.layout.bubble_with_icon,null);
        icon = findById(view,R.id.bubble_icon);
        text = findById(view,R.id.bubble_text);
        icon.setImageDrawable(date);
        text.setText(movie.getDate());
        infobox.addView(view);

        view = inflater.inflate(R.layout.bubble_with_icon,null);
        icon = findById(view,R.id.bubble_icon);
        text = findById(view,R.id.bubble_text);
        icon.setImageDrawable(rating);
        text.setText(movie.getMpaa());
        infobox.addView(view);


    }
    private void populateCast(LinearLayout castScroller) {
        final TextView textDir = findById(cirlceDirector,R.id.circle_text);
        textDir.setText(movie.getDirector().get(0));
        cirlceDirector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CircleImageView overlay = findById(cirlceDirector,R.id.circle_overlay);

                if (overlay.getAlpha()==0){
                    animation.fadeIn(overlay);
                    animation.fadeIn(textDir);
                } else {
                    animation.fadeOut(overlay);
                    animation.fadeOut(textDir);
                }

            }
        });
        CircleImageView image = findById(cirlceDirector,R.id.circle_image);
        Glide.with(this).load(movie.getDirectorPhotoURL().get(0)).into(image);
        image = null;

        int size = movie.getActors().size()+1;
        int childs = castScroller.getChildCount();
        for (int i = 1; i < childs; i++) {
            if (size>i){
                View view = castScroller.getChildAt(i);
                final CircleImageView overlay = findById(view,R.id.circle_overlay);
                final TextView text = findById(view,R.id.circle_text);
                image = findById(view,R.id.circle_image);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (overlay.getAlpha()==0){
                            animation.fadeIn(overlay);
                            animation.fadeIn(text);
                        } else {
                            animation.fadeOut(overlay);
                            animation.fadeOut(text);
                        }

                    }
                });
                text.setText(movie.getActors().get(i-1));
                Glide.with(this).load(movie.getActorsPhotoURL().get(i-1)).into(image);
            } else {
                while (castScroller.getChildCount() != size)
                    castScroller.removeViewAt(i);
                return;
            }
        }
    }
    private void populateGenre(List<String> list, LinearLayout scroller) {
        String[] items = movie.getArrAsString(list).split(", ");
        LayoutInflater inflater = LayoutInflater.from(this);
            for (String item: items){
                View view = inflater.inflate(R.layout.bubble,null);
                ((TextView)findById(view,R.id.bubble_text)).setText(item);
                setGenreBubbleColor((CardView)findById(view,R.id.card_bubble),item);
                scroller.addView(view);
        }
    }

    private void setGenreBubbleColor(CardView card, String text) {
        switch (text){
            case "боевик": card.setCardBackgroundColor(boevik);break;
            case "военный": card.setCardBackgroundColor(voennyi);break;
            case "драма": card.setCardBackgroundColor(drama);break;
            case "комедия": card.setCardBackgroundColor(komediya);break;
            case "криминал": card.setCardBackgroundColor(kriminal);break;
            case "мелодрама": card.setCardBackgroundColor(melodrama);break;
            case "мультфильм": card.setCardBackgroundColor(multfilm);break;
            case "приключения": card.setCardBackgroundColor(priklucheniya);break;
            case "триллер": card.setCardBackgroundColor(triller);break;
            case "ужасы": card.setCardBackgroundColor(uzhasy);break;
            case "фантастика": card.setCardBackgroundColor(fantastika);break;
        }
    }

    private void loadMovie(){
        Bundle bundle = getIntent().getExtras();
        int id = 0;
        if (bundle!=null){
            id = bundle.getInt("pos",0);
        }
        movie = movies.get(id);
    }

    private void initComment(final LayoutInflater inflater){
        Comment.init();
        commentLayout = inflater.inflate(R.layout.card_comment_add, null);
        commentLayout.setY(3000);
        parent.addView(commentLayout);
        TextView user = findById(commentLayout,R.id.comment_user);
        user.setText(UserInfo.getUsername());
        findById(commentLayout,R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                respondComment = null;
                animation.fadeOut(overlay);
                animation.rotationSlideOutFromBottom(commentLayout);
                enableScrolling();
            }
        });

        findById(commentLayout,R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                final Comment comment = new Comment(movie,respondComment,inflater.inflate(R.layout.card_comment_feed, null));
                animation.fadeOut(overlay);
                animation.rotationSlideOutFromBottom(commentLayout).setOnAnimationEnd(new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        enableScrolling();

                        // TODO: 19/12/2016 revise
                        scroller.smoothScrollTo(0, (int) (comment.getView().getY()+cardLayout.getHeight()));

//                        scroller.fullScroll(View.FOCUS_DOWN);
                    }
                });
                EditText edit = findById(commentLayout,R.id.comment_text);
                comment.setText(edit);

                comment.setRespondListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        respondComment = comment;
                        showComment();
                    }
                });

                CardView card = findById(comment.getView(),R.id.comment_card);
                comment.setResponseMargin(card);

                if (respondComment==null)
                    commentFeed.addView(comment.getView());
                else {
                    int index = commentFeed.indexOfChild(respondComment.getView())+respondComment.getThreadLength();
//                    index++;
                    commentFeed.addView(comment.getView(),index);
                    Log.d("happy", "pos "+String.valueOf(index));
                    respondComment = null;
                }
            }
        });
    }

    @OnClick(R.id.add_comment) void showComment(){
        disableScrolling();
        showKeyboard();
        animation.fadeIn(overlay);
        animation.rotationSlideInFromBottom(commentLayout,parent.getHeight()/3);
    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } else Toast.makeText(this, "FAILED TO CLOSE KEYBOARD", Toast.LENGTH_SHORT).show();
    }
    private void showKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

//    public class GestureListener extends GestureDetector.SimpleOnGestureListener {
//
//        @Override
//        public boolean onSingleTapConfirmed(MotionEvent e) {
//
//
//            return false;
//        }
//
//        @Override
//        public boolean onDoubleTap(MotionEvent e) {
//
//            return false;
//        }
//
//        @Override
//        public void onLongPress(MotionEvent e) {
////            animation.enterRateState(card, (int) e.getX());
//        }
//    }

}
