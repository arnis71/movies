package ru.arnis.movies.model;

import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import ru.arnis.movies.R;

import static butterknife.ButterKnife.findById;

/**
 * Created by arnis on 18/12/2016.
 */

public class Comment {
    private static List<Comment> comments;

    public static void init(){
        comments = new ArrayList<>();
    }

    private static void incThreadLength(Comment comment){
        Comment respond;
        while ((respond=comment.getRespondTo())!=null){
            respond.incThreadLength();
            comment = respond;
        }
    }

    private String ID;
    private int tier;
    private String text;
    private String user;
    private double userRating;
    private String movieID;
    private int threadLength;
    private View view;
    private Comment respondTo;

    public Comment(Movie movie, Comment respondTo, View commentView) {
        this.respondTo = respondTo;
        this.movieID = movie.getID();
        this.userRating = movie.getUserRating();
        view = commentView;
        setUser();
        threadLength = 0;
        comments.add(this);
        if (respondTo!=null){
            tier = respondTo.getTier()+1;
            incThreadLength(this);
        }
    }

    public Comment getRespondTo() {
        return respondTo;
    }

    public void setRespondTo(Comment respondTo) {
        this.respondTo = respondTo;
    }

    public void incThreadLength(){
        threadLength++;
    }

    public int getThreadLength() {
        return threadLength;
    }

    public void setResponseMargin(CardView cardView){
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(tier*50,0,0,0);
        cardView.setLayoutParams(params);
    }
    public void setRespondListener(View.OnClickListener listener){
        Button response = ButterKnife.findById(view, R.id.response);
        response.setOnClickListener(listener);
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMovieID() {
        return movieID;
    }

    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    public int getTier() {
        return tier;
    }

    public void incTier() {
        this.tier++;
    }

    public String getText() {
        return text;
    }

    public void setText(EditText edit) {
        TextView text = findById(view,R.id.comment_text);
        this.text = edit.getText().toString();
        text.setText(this.text);
        edit.getText().clear();
    }

    public String getUser() {
        return user;
    }

    public void setUser() {
        this.user = UserInfo.getUsername();
        TextView user = findById(view,R.id.comment_user);
        user.setText(getUser());
    }

    public double getUserRating() {
        return userRating;
    }

    public void setUserRating(double userRating) {
        this.userRating = userRating;
    }
}
