package ru.arnis.movies.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.arnis.movies.MovieDataConverter;


/**
 * Created by arnis on 15/11/2016.
 */

public class Parameter {
    public static final int MPAA = 0;
    public static final int GENRE = 1;
    public static final int ACTORS = 2;
    public static final int DIRECTORS = 3;
    public static final int KEYWORDS = 4;
    public static final int COUNTRIES = 5;


    private static ArrayList<ArrayList<Parameter>> params = new ArrayList<>();
    public static void reset(){
        params.clear();
    }


    private String name;
    private float rating;

    private Parameter(String name) {
        this.name = name;
        rating = 0;
    }

    public static void newEntry(Movie movie){
        List<String> listParam;
        for (int i = 0; i < 6; i++) {
            listParam = getMovieParamAtIter(movie,i);
            if (listParam!=null){
                for (String param: listParam) {
                    Parameter parameter = getParam(i, param);
                    if (parameter==null)
                        parameter = addParameter(i, param);


                    parameter.rate(movie.getUserRating());
                    Log.d("happy", "Parameter - " + param + " rated at " + String.format("%.1f", movie.getUserRating()) + " and now has a rating of " + String.format("%.1f", parameter.getRating()));
                }
            }
        }
    }
    public static void newDBentry(int type, String name, float rating){
        while (params.size()<=type)
            params.add(new ArrayList<Parameter>());
        params.get(type).add(new Parameter(name).setRating(rating));
    }
    public static ArrayList<Parameter> getParamRow(int type){
        if (params.size()>type)
            return params.get(type);
        else return new ArrayList<>();
    }
    private static Parameter addParameter(int index,String name){
        Parameter param = new Parameter(name);
        while (params.size()<=index)
            params.add(new ArrayList<Parameter>());
        params.get(index).add(param);
        return param;
    }
    private static Parameter getParam(int index, String name){
        if (params.size()>index)
            for (Parameter param: params.get(index)){
                if (param.getTitle().equals(name))
                    return param;
            }
        return null;
    }
    private static List<String> getMovieParamAtIter(Movie movie,int iter){
        switch (iter){
            case MPAA: if (movie.getMpaa()!=null){List<String> temp = new ArrayList<>();temp.add(movie.getMpaa()); return temp;} else return null;
            case GENRE: return movie.getGenre();
            case ACTORS: return movie.getActors();
            case DIRECTORS: return movie.getDirector();
            case KEYWORDS: return movie.getKeywords();
            case COUNTRIES: return movie.getCountries();
            default: return null;
        }
    }

    public static double[] createParamVector(){
        double[] vector = new double[MovieDataConverter.INPUT_FIELDS];
        int vecSch = 0;
        for (int i = 0; i < 6; i++) {
            if (i==0||i==1||i==5){
                for (Parameter parameter: params.get(i))
                    vector[vecSch++]=parameter.getRating();
            }
        }
        return vector;
    }

    public static double[] createSatisfactionVector(Movie movie){
        double[] vector = new double[MovieDataConverter.INPUT_FIELDS];
        int vecSch = 0;
        for (int i = 0; i < 6; i++)
            if (i==0||i==1||i==5){
            List<String> paramList = getMovieParamAtIter(movie, i);
            if (paramList!=null) {
                for (String param : paramList) {
                    Parameter parameter = getParam(i, param);
                    if (parameter != null) {
                        vector[vecSch]=parameter.getRating();
                        Log.d("happy", "satisfaction for "+ parameter.getTitle() +" is "+String.valueOf(parameter.getRating()));
                    }
                }
            }
        }
        return vector;
    }
    private static double[] getValueAsVector(float paramValue){
        if (paramValue==0)
            return new double[]{.2,.2,.2,.2,.2};
        else if (paramValue<.2)
            return new double[]{1,0,0,0,0};
        else if (paramValue<.4)
            return new double[]{0,1,0,0,0};
        else if (paramValue<.6)
            return new double[]{0,0,1,0,0};
        else if (paramValue<.8)
            return new double[]{0,0,0,1,0};
        else if (paramValue<1.2)
            return new double[]{0,0,0,0,1};

        return new double[]{.2,.2,.2,.2,.2};
    }

    public float getRating() {
        return rating;
    }
    private Parameter setRating(float rating) {
        this.rating = rating;
        return this;
    }
    private void rate(float value) {
        if (rating!=0)
            rating = (rating+value)/2;
        else rating = value;
    }
    public String getTitle() {
        return name;
    }
}
