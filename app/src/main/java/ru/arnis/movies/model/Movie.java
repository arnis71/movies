package ru.arnis.movies.model;

import java.util.List;

/**
 * Created by arnis on 10/11/2016.
 */

public class Movie {
    private String id;
    private String posterURL;
    private String titleRUS;
    private String titleENG;
    private String date;
    private List<String> genre;
    private String mpaa;
    private List<String> actors;
    private List<String> director;
    private List<String> keywords;
    private String budget;
    private String duration;
    private List<String> countries;
    private List<String> actorsPhotoURL;
    private List<String> directorPhotoURL;
    private String description;
    private float userRating;
    private double nnRating[];

    public Movie() {
    }

//    void setID(String id) {
//        this.id = id;
//        posterURL = POSTER_BASE + id +".jpg";
//    }

//    @Deprecated
//    void setPosterURL(String posterURL) {
//        this.posterURL = posterURL;
//    }
//    void setTitleRUS(String titleRUS) {
//        this.titleRUS = titleRUS;
//    }
//    void setTitleENG(String titleENG) {
//        this.titleENG = titleENG;
//    }
//    void setDate(String date) {
//        this.date = date;
//    }
//    void setGenre(String genre) {
//        if (genre!=null) {
//            genre = genre.replace("\"", "");
//            this.genre = genre.split(",");
//        } else
//            this.genre = new String[]{"n/a"};
//    }
//    void setGenre(Map<String,String> genre) {
//        this.genre = genre;
//    }
//    void setKeywords(Map<String,String> keywords) {
//        this.keywords = keywords;
//    }
//    void setActors(Map<String,String> actors) {
//        this.actors = actors;
//    }
//    void setDirector(Map<String,String> director) {
//        this.director = director;
//    }
//    void setProducer(Map<String,String> producer) {
//        this.producer = producer;
//    }
//    void setBudget(String budget) {
//        if (budget!=null)
//            this.budget = budget.replace("&nbsp;","");
//        else this.budget = "n/a";
//    }
//    void setDuration(String duration) {
//        this.duration = duration;
//    }
//    void setMpaa(String mpaa) {
//        this.mpaa = mpaa;
//    }


    public double[] getNnRating() {
        return nnRating;
    }

    public void setNnRating(double[] nnRating) {
        this.nnRating = nnRating;
    }

//    public void setCountries(Object countries) {
//        if (countries instanceof ArrayList){
//            ArrayList<String> wtf = (ArrayList<String>) countries;
//            this.countries = new HashMap<>();
//            for (int i = 0; i < wtf.size(); i++) {
//                String str = wtf.get(i);
//                if (str!=null)
//                    this.countries.put(Integer.toString(i),str);
//            }
//        } else this.countries = (HashMap<String, String>) countries;
//    }
//    public void setGenre(Object genre) {
//        if (genre instanceof ArrayList){
//            ArrayList<String> wtf = (ArrayList<String>) genre;
//            this.genre = new HashMap<>();
//            for (int i = 0; i < wtf.size(); i++) {
//                String str = wtf.get(i);
//                if (str!=null)
//                    this.genre.put(Integer.toString(i),str);
//            }
//        } else this.genre = (HashMap<String, String>) genre;
//    }
//    public void setMpaa(Object mpaa) {
//        if (mpaa instanceof ArrayList){
//            ArrayList<String> wtf = (ArrayList<String>) mpaa;
//            this.mpaa = new HashMap<>();
//            for (int i = 0; i < wtf.size(); i++) {
//                String str = wtf.get(i);
//                if (str!=null)
//                    this.mpaa.put(Integer.toString(i),str);
//            }
//        } else this.mpaa = (HashMap<String, String>) mpaa;
//    }

    public void setUserRating(float userRating) {
        this.userRating = userRating;
        Parameter.newEntry(this);
    }
    public float getUserRating() {
        return userRating;
    }
    private double[] getRatingAsArray(){
        if (userRating<.2)
            return new double[]{1,0,0,0,0};
        else if (userRating<.4)
            return new double[]{0,1,0,0,0};
        else if (userRating<.6)
            return new double[]{0,0,1,0,0};
        else if (userRating<.8)
            return new double[]{0,0,0,1,0};
        else if (userRating<1.2)
            return new double[]{0,0,0,0,1};

        return null;
    }
//    @Exclude
    public List<String> getCountries() {
        return countries;
    }
    public String getID() {
        return id;
    }
    public String getPosterURL() {
        return posterURL;
    }
    public String getTitleRUS() {
        return titleRUS;
    }
    public String getTitleENG() {
        return titleENG;
    }
    public String getDate() {
        return date;
    }
//    @Exclude
    public List<String> getGenre() {
        return genre;
    }
    public List<String> getKeywords() {
        return keywords;
    }
    public List<String> getActors() {
        return actors;
    }
    public List<String> getDirector() {
        return director;
    }
    public String getBudget() {
        if (budget.equals("n/a"))
            return "0";
        return budget;
    }
    public String getDuration() {
        return duration;
    }
//    @Exclude
    public String getMpaa() {
        return mpaa;
    }
    public String getArrAsString(List<String> arr) {
        String res="";
        int i=0;
        if (arr!=null) {
            String separator = ", ";
            for (String str: arr) {
                if (i++ == arr.size() - 1)
                    separator = "";
                res += str + separator;
            }
        }
        return res;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getActorsPhotoURL() {
        return actorsPhotoURL;
    }

    public void setActorsPhotoURL(List<String> actorsPhotoURL) {
        this.actorsPhotoURL = actorsPhotoURL;
    }

    public List<String> getDirectorPhotoURL() {
        return directorPhotoURL;
    }

    public void setDirectorPhotoURL(List<String> directorPhotoURL) {
        this.directorPhotoURL = directorPhotoURL;
    }
}
//    public String getActorsAsString(){
//        String res="";
//        if (actors!=null) {
//            String separator = ", ";
//            for (int i = 0; i < actors.length; i++) {
//                if (i == actors.length - 1)
//                    separator = "";
//                res += actors[i] + separator;
//            }
//        }
//        return res;
//    }