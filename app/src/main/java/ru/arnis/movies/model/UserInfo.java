package ru.arnis.movies.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by arnis on 28/12/2016.
 */

public class UserInfo {
    private static final String USER_DB = "user";
    private static final String FIRST_LAUNCH = "first_launch";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";

    private boolean firstLaunch;
    private static String username;
    private String email;
    private String pass;

    public UserInfo() {
        username = "test";
    }

    public UserInfo(String username, String email, String pass) {
        username = username;
        this.email = email;
        this.pass = pass;
    }

    public static UserInfo restore(Context context){
        UserInfo userInfo = new UserInfo();
        SharedPreferences prefs = context.getSharedPreferences(USER_DB,Context.MODE_PRIVATE);
        userInfo.setFirstLaunch(prefs.getBoolean(FIRST_LAUNCH,true));
        return userInfo;
    }

    public boolean isFirstLaunch() {
        return firstLaunch;
    }

    public void setFirstLaunch(boolean firstLaunch) {
        this.firstLaunch = firstLaunch;
    }

    public static String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isAnon(){
        return username == null;
    }
}
