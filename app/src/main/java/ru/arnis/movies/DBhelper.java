package ru.arnis.movies;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.arnis.movies.model.Parameter;

/**
 * Created by arnis on 15/11/2016.
 */

public class DBhelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "db.db";
    public static final String TABLE_KEYWORDS = "keywords";
    public static final String TABLE_ACTORS = "actors";
    public static final String TABLE_DIRECTORS = "directors";
    public static final String TABLE_MPAA = "mpaa";
    public static final String TABLE_GENRE = "genre";
    public static final String TABLE_COUNTRY = "country";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_RATING = "rating";

    private ExecutorService thread;
    private int sch;

    public DBhelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        thread = Executors.newSingleThreadExecutor();
        sch=0;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (int i = 0; i < 6; i++) {
            createTable(getTableAtIteration(i),sqLiteDatabase);
        }
    }
    private void createTable(String name, SQLiteDatabase sqLiteDatabase){
        String query = "CREATE TABLE " +name + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_TITLE + " TEXT," +
                COLUMN_RATING + " REAL" + ");";
        sqLiteDatabase.execSQL(query);
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        for (int j = 0; j < 6; j++) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ getTableAtIteration(j));
        }
        onCreate(sqLiteDatabase);

    }
    public void clearTables(){
        SQLiteDatabase db = getWritableDatabase();
        for (int i = 0; i < 6; i++) {
            db.execSQL("DROP TABLE IF EXISTS "+ getTableAtIteration(i));
        }
        onCreate(db);
    }


    public void restoreParams() {
        execute(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 6; i++) {
                        SQLiteDatabase database = getReadableDatabase();
                        Cursor cursor = database.query(getTableAtIteration(i), new String[]{/*COLUMN_ID, */COLUMN_TITLE, COLUMN_RATING}, null, null, null, null, null);
                        cursor.moveToFirst();
                        String id, title;
                        float rating;
                        for (int j = 0; j < cursor.getCount(); j++) {
                            Log.d("db", "restoring from database");
                            title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
                            rating = cursor.getFloat(cursor.getColumnIndex(COLUMN_RATING));
                            if (!cursor.isLast())
                                cursor.moveToNext();
                            Parameter.newDBentry(i, title, rating);
                        }
                        database.close();
                        cursor.close();
                    }
                } catch (SQLiteException e){
                    e.printStackTrace();
                }
            }
        });

    }
    public void backUpParams(){
        clearTables();
        execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 6; i++) {
                    SQLiteDatabase db = getWritableDatabase();
                    ContentValues contentValues = new ContentValues();
                    for (Parameter parameter : Parameter.getParamRow(i)){
                        Log.d("db", "saving to database");
                        contentValues.clear();
                        contentValues.put(COLUMN_TITLE, parameter.getTitle());
                        contentValues.put(COLUMN_RATING, parameter.getRating());
                        try{
                            db.insert(getTableAtIteration(i), null, contentValues);
                        }
                        catch (SQLiteConstraintException e){
                            e.printStackTrace();
                        }
                    }
                    db.close();
                }
            }
        });

    }

    private void execute(Runnable runnable){
        thread.execute(runnable);
    }

    private String getTableAtIteration(int i){
        switch (i){
            case Parameter.MPAA: return TABLE_MPAA;
            case Parameter.GENRE: return TABLE_GENRE;
            case Parameter.ACTORS: return TABLE_ACTORS;
            case Parameter.DIRECTORS: return TABLE_DIRECTORS;
            case Parameter.KEYWORDS: return TABLE_KEYWORDS;
            case Parameter.COUNTRIES: return TABLE_COUNTRY;
            default: return "";
        }
    }
}
