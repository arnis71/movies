package ru.arnis.movies;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.arnis.movies.model.Movie;

/**
 * Created by arnis on 12/11/2016.
 */

public class MovieDataConverter {
    public static final int INPUT_FIELDS = 30+3+3;//+5+3+3+3;
    public static final int OUTPUT_FIELDS = 1;
    private Context context;

    private ArrayList<String> genres;
    private ArrayList<String> MPAA;
    private ArrayList<String> countries;

    private Range durationRange;
    private Range budgetRange;

    private OneHot oneHot;


    public MovieDataConverter(Context context) {
        this.context = context;
        load();
        oneHot = new OneHot();
        durationRange = new Range(Range.DURATION_LO,Range.DURATION_HI);
        budgetRange = new Range(Range.BUDGET_LO,Range.BUDGET_HI);
    }

    private void load(){
        genres = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.genres)));
        countries = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.countries)));
        MPAA = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.MPAA)));
    }

    public double[] convertMovieData(Movie movie){
        DynamicList list = new DynamicList();

        list.addValues(oneHot.byValues(movie.getGenre(),genres));
        list.addValues(oneHot.byValuesStrict(movie.getCountries(),countries));
//        list.addValues(oneHot.byValue(movie.getMpaa(),MPAA));

        String str = movie.getDuration();
        if (str!=null){
            int durIndex = durationRange.inRange(Integer.parseInt(str));
            list.addValues(oneHot.byIndex(durIndex,OneHot.TRIPLET));
        }
//        str = movie.getBudget();
//        if (str!=null){
//            int budIndex = budgetRange.inRange(Integer.parseInt(str));
//            list.addValues(oneHot.byIndex(budIndex,OneHot.TRIPLET));
//        }


        return list.toArray();
    }
    public double[] convertMovieRating(Movie movie){
        double[] res =  new double[OUTPUT_FIELDS];
//        Range range = new Range(0,1);
        res[0/*range.inRange(movie.getUserRating())*/]=movie.getUserRating(); //1;
        return res;
    }
//    public double[]

    private class OneHot{
        static final int TRIPLET = 3;
        OneHot() {
        }

        double[] byIndex(int index, int length){
            double[] res = new double[length];
            res[index] = 1;
            return res;
        }

        double[] byValue(String value, List<String> list){
            double[] res = new double[list.size()];
            int index = list.indexOf(value);
            if (index!=-1)
                res[index] = 1;
            return res;
        }
        double[] byValues(List<String> values, List<String> list){
            double[] res = new double[list.size()];
            int index;
            for (String value:values){
                index = list.indexOf(value);
                if (index!=-1)
                    res[index] = 1;
            }
            return res;
        }
        double[] byValuesStrict(List<String> values, List<String> list){
            double[] res = new double[list.size()];
            int index;
            for (String value:values){
                index = list.indexOf(value);
                if (index!=-1)
                    res[index] = 1;
                else res[res.length-1]=1;
            }
            return res;
        }
    }
    private class DynamicList{
        private ArrayList<Double> list;

        DynamicList() {
            list = new ArrayList<>();
        }

        void addValues(double... values){
            for (double d:values)
                list.add(d);
        }

        double[] toArray(){
            return toPrimitive(list.toArray());
        }
        private double[] toPrimitive(Object[] array) {
            if (array == null) {
                return null;
            } else if (array.length == 0) {
                return new double[]{};
            }
            final double[] result = new double[array.length];
            for (int i = 0; i < array.length; i++) {
                result[i] = (double) array[i];
            }
            return result;
        }

    }
    private class Range{
        public static final int DURATION_LO = 60;
        public static final int DURATION_HI = 180;
        public static final int BUDGET_LO = 100000;
        public static final int BUDGET_HI = 400000000;
        private double lo;
        private double mid;
        private double hi;

        Range(double lo,double hi) {
            this.lo = lo;
            this.mid = (lo+hi)/2;
            this.hi = hi;
        }

        int inRange(double value){
            double loDif = Math.abs(value-lo);
            double midDif = Math.abs(value-mid);
            double hiDif = Math.abs(value-hi);
            double res = Math.min(loDif,Math.min(midDif,hiDif));
            if (res==loDif)
                return 0;
            else if (res==midDif)
                return 1;
            else if (res==hiDif)
                return 2;
            else return -1;
        }
    }
}
