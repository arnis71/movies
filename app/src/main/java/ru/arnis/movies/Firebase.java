package ru.arnis.movies;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import ru.arnis.movies.components.OnCompleteListener;
import ru.arnis.movies.model.Movie;

import static ru.arnis.movies.MainActivity.PREMIERES;
import static ru.arnis.movies.MainActivity.TOP250;


/**
 * Created by arnis on 13.09.2016.
 */
public class Firebase {

//    private Auth auth;
//    private Analytics analytics;
//    private Remote remote;

    private DB db;

    public Firebase(Context context) {
//        analytics = new Analytics(context);
//        auth = new Auth((Activity) context);
//        remote = new Remote();
        db = new DB();
    }

//    public Auth auth() {
//        return auth;
//    }
//    public Analytics analytics() {
//        return analytics;
//    }
//    public Remote remote(){return remote;}

    public DB db(){return db;}

//    public static class Auth{
//        private FirebaseAuth auth;
//        private FirebaseAuth.AuthStateListener authListener;
//        private Activity context;
//
//        private Auth(final Activity context) {
//            auth = FirebaseAuth.getInstance();
//            this.context = context;
//            authListener = new FirebaseAuth.AuthStateListener() {
//                @Override
//                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//
//                }
//            };
//        }
//
//        public void signInAnonymous(){
//            auth.signInAnonymously().addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
//                @Override
//                public void onComplete(@NonNull Task<AuthResult> task) {
//                    if (task.isSuccessful()){
//                        Log.d("happy", "user logged in anonymously");
//                        enterApp();
//                    } else Log.d("happy", "fail login anonymously");
//                }
//            });
//        }
//        public void signIN(UserInfo userInfo){
//                auth.signInWithEmailAndPassword(userInfo.getEmail(), userInfo.getPassword()).addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            Log.d("happy", "user logged in");
//                            enterApp();
//                        } else Log.d("happy", "user login fail");
//                    }
//                });
//        }
//        public void vkAuth(final UserInfo userInfo){
//                auth.createUserWithEmailAndPassword(userInfo.getEmail(), userInfo.getPassword()).addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task task) {
//                        signIN(userInfo);
//                    }
//                });
//
//
//        }
//        public void signUP(final UserInfo userInfo){
//                auth.createUserWithEmailAndPassword(userInfo.getEmail(), userInfo.getPassword()).addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task task) {
//                        if (task.isSuccessful()) {
//                            Log.d("happy","user added");
//                            signIN(userInfo);
//                            if (userInfo.has(UserInfo.NICKNAME))
//                                updateUserInfo(auth.getCurrentUser(),userInfo);
//                        } else Log.d("happy","user not added");
//                    }
//                });
//
//        }
//        public void signUpAnon(UserInfo userInfo){
//            AuthCredential credential = EmailAuthProvider.getCredential(userInfo.getEmail(),userInfo.getPassword());
//            getUser().linkWithCredential(credential).addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
//                @Override
//                public void onComplete(@NonNull Task<AuthResult> task) {
//                    if (task.isSuccessful()){
//                        Log.d("happy", "anon linked");
//                    } else Log.d("happy", "anon not linked");
//                }
//            });
//        }
//        public FirebaseUser getUser(){
//            return auth.getCurrentUser();
//        }
//        protected void updateUserInfo(FirebaseUser user,UserInfo userInfo){
//            if (user!=null) {
//                UserProfileChangeRequest.Builder profileUpdates = new UserProfileChangeRequest.Builder();
//                if (userInfo.has(UserInfo.NICKNAME))
//                    profileUpdates.setDisplayName(userInfo.getNickname());
////                if (photoURL!=null&&!photoURL.equals(""))
////                    profileUpdates.setPhotoUri(Uri.parse("https://example.com/jane-q-user/profile.jpg"));
//
//                user.updateProfile(profileUpdates.build()).addOnCompleteListener(context, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()){
//                            Log.d("happy", "user info updated");
//                        } else Log.d("happy", "error user info not updated");
//                    }
//                });
//            }
//        }
//        public void addListener(){
//            auth.addAuthStateListener(authListener);
//        }
//        public void removeListener(){
//            if (authListener!=null)
//                auth.removeAuthStateListener(authListener);
//        }
//        public void signOUT(boolean sendToLogin){
//            auth.signOut();
//            if (sendToLogin){
//                loginPage();
//            }
//        }
//        private void enterApp(){
//            Intent intent = new Intent(context,Main.class);
//            context.startActivity(intent);
//        }
//        private void loginPage(){
//            Intent intent = new Intent(context,Login.class);
//            context.startActivity(intent);
//        }
//        public void checkUserActive(){
//            if (getUser()==null)
//                loginPage();
//        }
//
//    }
//
//    public static class Analytics {
//        private Bundle bundle;
//        private FirebaseAnalytics analytics;
//
//        private Analytics(Context context) {
//            analytics = FirebaseAnalytics.getInstance(context);
//        }
//
//        public FirebaseAnalytics getAnalytics() {
//            return analytics;
//        }
//
//        public Analytics createBundle() {
//            bundle = new Bundle();
//            return this;
//        }
//
//        public Analytics putString(String param, String data) {
//            bundle.putString(param, data);
//            return this;
//        }
//
//        public Analytics putValue(String param, long data) {
//            bundle.putLong(param, data);
//            return this;
//        }
//
//        public void logEvent(String event) {
//            analytics.logEvent(event, bundle);
//        }
//    }
//
//    public static class Remote{
//        public static final String TICKETS_UP = "ticketsUp";
//        public static final String DEFAULT_CACHE = "default_cache";
//        public static final String CONSTANT_CACHE = "const_cache";
//
//        FirebaseRemoteConfig remoteConfig;
//
//        private Remote() {
//            remoteConfig = FirebaseRemoteConfig.getInstance();
//            remoteConfig.setDefaults(R.xml.default_values);
//        }
//
//        public void fetch(String cacheType){
//            long seconds;
//            switch (cacheType){
//                case DEFAULT_CACHE: seconds = 43200;break;
//                case CONSTANT_CACHE: seconds = 1;break;
//                default: seconds = 43200;break;
//            }
//            remoteConfig.fetch(seconds).addOnCompleteListener(new OnCompleteListener<Void>() {
//                @Override
//                public void onComplete(@NonNull Task<Void> task) {
//                    if (task.isSuccessful()){
//                        remoteConfig.activateFetched();
//                        Log.d("happy", "fetching completed "+remoteConfig.getBoolean(TICKETS_UP));
//                    }
//                    else Log.d("happy", "fetching error");
//                }
//            });
//        }
//
//        public boolean getTicketsAvaliablity(){
//            return remoteConfig.getBoolean(TICKETS_UP);
//        }
//    }

    public static class DB{
        private static final String DB_VERSION = "version_0_1";
        private static final int PAGING = 5;
        private static final int THIS_WEEK = 0;
        private static final String MOVIES = "movies";

        private FirebaseDatabase database;
        private DatabaseReference movies;
        private DatabaseReference top250;
        private DatabaseReference premieres;

        private DatabaseReference active;
        private Iterator<DataSnapshot> iterator;
        private ArrayList<String> avaliableDates;

        private OnCompleteListener onMovieLoaded;
        public void setOnMovieLoaded(OnCompleteListener onMovieLoaded) {
            this.onMovieLoaded = onMovieLoaded;
        }

        private DB() {
            avaliableDates = new ArrayList<>();
            database = FirebaseDatabase.getInstance();
            movies = database.getReference(DB_VERSION).child(MOVIES);
            top250 = movies.child(TOP250);
            premieres = movies.child(PREMIERES);

        }

        public void init(boolean firstLaunch){
            Log.d("happy", "INIT LOADING");
            if (!firstLaunch) {
                premieres.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (Object o : dataSnapshot.getChildren())
                            avaliableDates.add(((DataSnapshot) (o)).getKey());

                        //Init loading
                        setMoviesToLoad(PREMIERES, THIS_WEEK);
                        load(PAGING);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                setMoviesToLoad(TOP250, -1);
                load(PAGING);
            }
        }

        public void setMoviesToLoad(String type, int index){
            reset();
            switch (type){
                case TOP250: active = top250;break;
                case PREMIERES: active = premieres.child(String.valueOf(index));break;
            }
        }

        public void reset(){
            iterator = null;
            active = null;
        }

        public void load(final int paging){
            if (active!=null)
                active.addListenerForSingleValueEvent(new ValueEventListener() {
                    int page;
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (iterator==null){
                            iterator = dataSnapshot.getChildren().iterator();
                            page = 0;
                        }
                        while (iterator.hasNext()){
                            DataSnapshot snap = iterator.next();
                            Movie movie = snap.getValue(Movie.class);
                            onMovieLoaded.onComplete(movie);
                            page++;
                            if (page == paging)
                                break;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        }
    }
}
