package ru.arnis.movies;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import io.codetail.animation.ViewAnimationUtils;
import ru.arnis.movies.model.Movie;
import ru.nn2.components.OnCompleteListener;

/**
 * Created by arnis on 11/11/2016.
 */

public class Animation {
    private static boolean animating;
    public static boolean isAnimating() {
        return animating;
    }
    public static void setAnimating(boolean animating) {
        Animation.animating = animating;
    }

    private RecyclerView recyclerView;
    private OnCompleteListener onAnimationEnd;
    public void setOnAnimationEnd(OnCompleteListener onAnimationEnd) {
        this.onAnimationEnd = onAnimationEnd;
    }

    private CardView rateCard;
    private View cardSlider;

    private View circle;
    private ImageView circleBack;
    private int circleX;
    private int circleOff;



    public Animation() {
        once = true;
    }

    public Animation(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        once = true;
    }

    CardView getRateCard() {
        return rateCard;
    }
    private void addCardSlider(int x) {
        cardSlider = rateCard.findViewById(R.id.stride);
        cardSlider.setVisibility(View.VISIBLE);
        setSliderAt(x);
    }
    private void setSliderAt(int x){
        cardSlider.setX(x-(cardSlider.getWidth()));
    }
    private void removeCard(){
        rateCard =null;
        if (cardSlider!=null)
            cardSlider.setVisibility(View.INVISIBLE);
        cardSlider=null;
    }
    public void enterRateState(CardView card, int atX){
        this.rateCard =card;
        addCardSlider(atX);
        init(atX,rateCard.getWidth());
        card.findViewById(R.id.background).animate().scaleY(.8f).scaleX(.8f).setInterpolator(new BounceInterpolator()).setDuration(1000).start();
    }
    public float leaveRateState(int atX){
        if (rateCard!=null)
            rateCard.findViewById(R.id.background).animate().scaleY(1f).scaleX(1f).setInterpolator(new BounceInterpolator()).setDuration(1000).start();
        removeCard();
        return (float) atX/ totalWidth;
    }

    private int startColorValue = 87;
    private int endColorValue = 198;
    private int sectors;
    private int stride;
    private int totalWidth;
//    private int[] red = new int[]{198,198,87};
//    private int[] yellow = new int[]{198,198,87};
//    private int[] green = new int[]{87,198,87};
    private int[][] sectorColor;
    private boolean once;

    private void init(int atX, int totalWidth){
        if (once) {
            this.totalWidth = totalWidth;
            sectors = (endColorValue - startColorValue) * 2;
            stride = this.totalWidth / sectors;

            sectorColor = new int[sectors][3];
            int sch = 0;
            for (int i = startColorValue; i < endColorValue; i++) {
                sectorColor[sch++] = new int[]{endColorValue, i, startColorValue};
            }
            for (int i = endColorValue; i > startColorValue; i--) {
                sectorColor[sch++] = new int[]{i, endColorValue, startColorValue};
            }
            once=false;
        }
        changeMovieRating(atX);
    }
    public void changeMovieRating(int x){
        if (rateCard!=null)
            changeMovieRatingColor(x);
        else changeMovieRatingFilterColor(x);
    }
    private void changeMovieRatingColor(int x){
        int currSector=x/stride;
        if (x%stride!=0)
            currSector++;
        setSliderAt(x);
        if (currSector<sectors)
            rateCard.setCardBackgroundColor(Color.rgb(sectorColor[currSector][0],sectorColor[currSector][1],sectorColor[currSector][2]));
        else rateCard.setCardBackgroundColor(Color.rgb(sectorColor[sectorColor.length-1][0],sectorColor[sectorColor.length-1][1],sectorColor[sectorColor.length-1][2]));
    }
    private void changeMovieRatingFilterColor(int x){
        int currSector=x/stride;
        if (x%stride!=0)
            currSector++;
        if (currSector < sectors && currSector > 0)
            circleBack.setColorFilter(Color.rgb(sectorColor[currSector][0], sectorColor[currSector][1],sectorColor[currSector][2]));
        else if (currSector>0)
            circleBack.setColorFilter(Color.rgb(sectorColor[sectorColor.length-1][0],sectorColor[sectorColor.length-1][1],sectorColor[sectorColor.length-1][2]));
        else if (currSector<0)
            circleBack.setColorFilter(Color.rgb(sectorColor[0][0],sectorColor[0][1],sectorColor[0][2]));
        else if (currSector == 0)
            circleBack.setImageResource(R.drawable.circle_stroke);
    }


    public void showPredicting(View spinBars, View overlay){
//        View spinBars = card.findViewById(R.id.spin_bars);
//        View overlay = card.findViewById(R.id.card_overlay);
        spinBars.setVisibility(View.VISIBLE);
        overlay.setVisibility(View.VISIBLE);
        animate(spinBars, overlay);
    }
    private void animate(final View spinBars, final View overlay){
        final View greenBar = spinBars.findViewById(R.id.green_bar);
        final View redBar = spinBars.findViewById(R.id.red_bar);
        final View yellowBar = spinBars.findViewById(R.id.yellow_bar);

        PropertyValuesHolder greenXin = PropertyValuesHolder.ofFloat(View.SCALE_X,0,1);
        PropertyValuesHolder greenYin = PropertyValuesHolder.ofFloat(View.SCALE_Y,0,1);
        PropertyValuesHolder greenAlpha = PropertyValuesHolder.ofFloat(View.ALPHA,0,1);
        ObjectAnimator greenXYin = ObjectAnimator.ofPropertyValuesHolder(greenBar,greenXin,greenYin,greenAlpha);
        greenXYin.setDuration(1500).setInterpolator(new OvershootInterpolator());

        PropertyValuesHolder redXin = PropertyValuesHolder.ofFloat(View.SCALE_X,0,1);
        PropertyValuesHolder redYin = PropertyValuesHolder.ofFloat(View.SCALE_Y,0,1);
        PropertyValuesHolder redAlpha = PropertyValuesHolder.ofFloat(View.ALPHA,0,1);
        ObjectAnimator redXYin = ObjectAnimator.ofPropertyValuesHolder(redBar,redXin,redYin,redAlpha);
        redXYin.setDuration(1500).setInterpolator(new OvershootInterpolator());
        redXYin.setStartDelay(300);

        PropertyValuesHolder yellowXin = PropertyValuesHolder.ofFloat(View.SCALE_X,0,1);
        PropertyValuesHolder yellowYin = PropertyValuesHolder.ofFloat(View.SCALE_Y,0,1);
        PropertyValuesHolder yellowAlpha = PropertyValuesHolder.ofFloat(View.ALPHA,0,1);
        ObjectAnimator yellowXYin = ObjectAnimator.ofPropertyValuesHolder(yellowBar,yellowXin,yellowYin,yellowAlpha);
        yellowXYin.setDuration(1500).setInterpolator(new OvershootInterpolator());
        yellowXYin.setStartDelay(600);


//        PropertyValuesHolder greenXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
//        PropertyValuesHolder greenYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
//        ObjectAnimator greenXYout = ObjectAnimator.ofPropertyValuesHolder(greenBar,greenXout,greenYout);
//        greenXYout.setDuration(1000).setInterpolator(new AccelerateInterpolator());
//
//        PropertyValuesHolder redXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
//        PropertyValuesHolder redYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
//        ObjectAnimator redXYout = ObjectAnimator.ofPropertyValuesHolder(redBar,redXout,redYout);
//        redXYout.setDuration(800).setInterpolator(new AccelerateInterpolator());
//
//        PropertyValuesHolder yellowXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
//        PropertyValuesHolder yellowYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
//        ObjectAnimator yellowXYout = ObjectAnimator.ofPropertyValuesHolder(yellowBar,yellowXout,yellowYout);
//        yellowXYout.setDuration(600).setInterpolator(new AccelerateInterpolator());

        ObjectAnimator green = ObjectAnimator.ofFloat(greenBar,View.ROTATION,0,720);
        green.setDuration(4000).setRepeatCount(ValueAnimator.INFINITE);
        green.setRepeatMode(ValueAnimator.RESTART);

        ObjectAnimator red = ObjectAnimator.ofFloat(redBar,View.ROTATION,0,-720);
        red.setDuration(4000).setRepeatCount(ValueAnimator.INFINITE);
        red.setRepeatMode(ValueAnimator.RESTART);

        ObjectAnimator yellow = ObjectAnimator.ofFloat(yellowBar,View.ROTATION,0,720);
        yellow.setDuration(4000).setRepeatCount(ValueAnimator.INFINITE);
        yellow.setRepeatMode(ValueAnimator.RESTART);

        ObjectAnimator alphaIn = ObjectAnimator.ofFloat(overlay,View.ALPHA,0,0.6f);
        alphaIn.setDuration(1000).setInterpolator(new AccelerateDecelerateInterpolator());

//        ObjectAnimator alphaOut = ObjectAnimator.ofFloat(overlay,View.ALPHA,0.6f,0);
//        alphaOut.setDuration(1000).setInterpolator(new AccelerateDecelerateInterpolator());

        AnimatorSet set = new AnimatorSet();
        set.play(green).with(red).with(yellow).with(alphaIn);
        set.play(greenXYin).with(green);
        set.play(yellowXYin).with(redXYin).with(greenXYin);
//        set.play(yellowXYout).with(redXYout).with(greenXYout).with(alphaOut).after(green);
        set.start();
//        set.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animator) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animator) {
//                hidePredicting(spinBars,overlay);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animator) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animator) {
//
//            }
//        });
    }
    public void hidePredicting(final View spinBars, final View overlay){
        final View greenBar = spinBars.findViewById(R.id.green_bar);
        final View redBar = spinBars.findViewById(R.id.red_bar);
        final View yellowBar = spinBars.findViewById(R.id.yellow_bar);

        PropertyValuesHolder greenXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
        PropertyValuesHolder greenYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
        ObjectAnimator greenXYout = ObjectAnimator.ofPropertyValuesHolder(greenBar,greenXout,greenYout);
        greenXYout.setDuration(1000).setInterpolator(new AccelerateInterpolator());

        PropertyValuesHolder redXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
        PropertyValuesHolder redYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
        ObjectAnimator redXYout = ObjectAnimator.ofPropertyValuesHolder(redBar,redXout,redYout);
        redXYout.setDuration(800).setInterpolator(new AccelerateInterpolator());

        PropertyValuesHolder yellowXout = PropertyValuesHolder.ofFloat(View.SCALE_X,1,0);
        PropertyValuesHolder yellowYout = PropertyValuesHolder.ofFloat(View.SCALE_Y,1,0);
        ObjectAnimator yellowXYout = ObjectAnimator.ofPropertyValuesHolder(yellowBar,yellowXout,yellowYout);
        yellowXYout.setDuration(600).setInterpolator(new AccelerateInterpolator());

        ObjectAnimator alphaOut = ObjectAnimator.ofFloat(overlay,View.ALPHA,0.6f,0);
        alphaOut.setDuration(1000).setInterpolator(new AccelerateDecelerateInterpolator());

        AnimatorSet set = new AnimatorSet();
        set.play(yellowXYout).with(redXYout).with(greenXYout).with(alphaOut);//.after(green);
        set.start();
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                spinBars.setVisibility(View.GONE);
                overlay.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }

    public void showAsistantReveal(final View reveal, final View asistantDialog){
        // get the center for the clipping circle_icon
        int cx = (reveal.getLeft() + reveal.getRight()) / 2;
        int cy = reveal.getBottom();

        // get the final radius for the clipping circle_icon
        int dx = Math.max(cx, reveal.getWidth() - cx);
        int dy = Math.max(cy, reveal.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // Android native animator
        Animator animator = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setDuration(1500).addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                asistantDialog.setVisibility(View.VISIBLE);
//                AnimationDrawable frameAnimation = (AnimationDrawable)reveal.getBackground();
//                frameAnimation.start();

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        reveal.setVisibility(View.VISIBLE);
        animator.start();
    }
    public void hideAsistantReveal(final View reveal, final LinearLayout asistantDialog){
        // get the center for the clipping circle_icon
        int cx = (reveal.getLeft() + reveal.getRight()) / 2;
        int cy = reveal.getBottom();

        // get the final radius for the clipping circle_icon
        int dx = Math.max(cx, reveal.getWidth() - cx);
        int dy = Math.max(cy, reveal.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // Android native animator
        Animator animator = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, finalRadius, 0);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.setDuration(500).addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                asistantDialog.setVisibility(View.INVISIBLE);
                reveal.setVisibility(View.INVISIBLE);
                asistantDialog.removeAllViews();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }


    private void showRatingReveal(final View reveal, final View textView){
        // get the center for the clipping circle_icon
        int cx = (reveal.getLeft() + reveal.getRight()) / 2;
        int cy = (reveal.getBottom() + reveal.getTop()) / 2;

        // get the final radius for the clipping circle_icon
        int dx = Math.max(cx, reveal.getWidth() - cx);
        int dy = Math.max(cy, reveal.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // Android native animator
        Animator animatorIN = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, 0, finalRadius);
        animatorIN.setInterpolator(new BounceInterpolator());
        animatorIN.setDuration(1200);

        Animator animatorOUT = ViewAnimationUtils.createCircularReveal(reveal, cx, cy, finalRadius, 0);
        animatorOUT.setInterpolator(new DecelerateInterpolator());
        animatorOUT.setDuration(1000);

        AnimatorSet set  = new AnimatorSet();
        set.play(animatorOUT).after(5000).after(animatorIN);
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                reveal.setVisibility(View.INVISIBLE);
                textView.setVisibility(View.INVISIBLE);
                onAnimationEnd.onComplete();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        reveal.setVisibility(View.VISIBLE);
        set.start();
    }
    public Animation setRatingTextColor(View view, double[] rating, boolean reveal) {
        TextView textRating = ((TextView)view.findViewById(R.id.rating_text));
        View textColor = view.findViewById(R.id.rating_color);
        int[] render = renderResult(rating);
        int color = render[0];
        String value = Integer.toString(render[1]);

        textRating.setVisibility(View.VISIBLE);
        textRating.setText(value);
        textColor.setBackgroundColor(color);
        if (reveal)
            showRatingReveal(textColor,textRating);

        return this;
    }

    private int[] renderResult(double[] result){
        int confidence=0;
        int color=0;
        double max = 0;
        int pos = 0;
        // TODO: 17/12/2016 can happen faster than results are loaded
        for (int i=0;i<result.length;i++)
            if (result[i]>max){
                max = result[i];
                pos = i;
            }
        //    private int[] red = new int[]{198,198,87};
        //    private int[] yellow = new int[]{198,198,87};
        //    private int[] green = new int[]{87,198,87};
        switch (pos){
            case 0:color = Color.rgb(endColorValue,startColorValue,startColorValue);confidence = (int) (max * 100);break;
//            case 1:color = Color.rgb(endColorValue,(startColorValue+endColorValue)/2,startColorValue);confidence = (int) (max * 100);break;
            case 1:color = Color.rgb(endColorValue,endColorValue,startColorValue);confidence = (int) (max * 100);break;
//            case 3:color = Color.rgb((startColorValue+endColorValue)/2,endColorValue,startColorValue);confidence = (int) (max * 100);break;
            case 2:color = Color.rgb(startColorValue,endColorValue,startColorValue);confidence = (int) (max * 100);break;
        }
        return new int[]{color,confidence};
    }


    public Animation exitRight() {
        for (int i = 0;i<recyclerView.getChildCount();i++){
            View view = recyclerView.getChildAt(i);
            if (i!=recyclerView.getChildCount()-1)
                view.animate().x(recyclerView.getWidth()).setInterpolator(new DecelerateInterpolator()).setStartDelay(i*150).setDuration(1200).start();
            else
                view.animate().x(recyclerView.getWidth()).setInterpolator(new DecelerateInterpolator()).setStartDelay(i*150).setDuration(1200)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                onAnimationEnd.onComplete();
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        }).start();
        }
        return this;
    }
    public Animation exitLeft() {
        for (int i = 0;i<recyclerView.getChildCount();i++){
            View view = recyclerView.getChildAt(i);
            if (i!=recyclerView.getChildCount()-1)
                view.animate().x(-recyclerView.getWidth()).setInterpolator(new DecelerateInterpolator()).setStartDelay(i*150).setDuration(1200).start();
            else
                view.animate().x(-recyclerView.getWidth()).setInterpolator(new DecelerateInterpolator()).setStartDelay(i*150).setDuration(1200)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                onAnimationEnd.onComplete();
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        }).start();
        }
        return this;
    }

    public void snapRecyclerView(){
        recyclerView.animate().y(0).setDuration(500).setInterpolator(new AccelerateInterpolator()).start();
    }

    public void revealDate(final TextView textView){
        textView.clearAnimation();
        ObjectAnimator animatorIN = ObjectAnimator.ofFloat(textView,View.Y,-textView.getHeight(),0);
        animatorIN.setInterpolator(new OvershootInterpolator());
        animatorIN.setDuration(1000);

        ObjectAnimator animatorOUT = ObjectAnimator.ofFloat(textView,View.Y,0,-textView.getHeight());
        animatorOUT.setInterpolator(new OvershootInterpolator());
        animatorOUT.setDuration(1000);

        textView.setVisibility(View.VISIBLE);
        AnimatorSet set = new AnimatorSet();
        set.play(animatorOUT).after(5000).after(animatorIN);
        set.start();
    }


    public void revealCircles(final View userCircle, final View NNcircle, ImageView userCircleBack, ImageView NNcircleBack, Movie movie, int width){
        int[] render = renderResult(movie.getNnRating());
        NNcircleBack.setColorFilter(render[0]);
        float userRating = movie.getUserRating();
        circleBack = userCircleBack;
        init((int) (width*userRating),width);

        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat(View.SCALE_X,0,1);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat(View.SCALE_Y,0,1);

        ObjectAnimator animatorNN = ObjectAnimator.ofPropertyValuesHolder(NNcircle,pvhX,pvhY);
        animatorNN.setDuration(1000);
        animatorNN.setInterpolator(new OvershootInterpolator());

        ObjectAnimator animatorUser = ObjectAnimator.ofPropertyValuesHolder(userCircle,pvhX,pvhY);
        animatorUser.setDuration(1000);
        animatorUser.setInterpolator(new OvershootInterpolator());
        animatorUser.setStartDelay(1000);

        AnimatorSet set = new AnimatorSet();
        set.play(animatorNN).after(animatorUser);
        set.start();
        animatorUser.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                userCircle.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animatorNN.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                NNcircle.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    public void setCircle(View circle, ImageView circleUserBack, int x) {
        this.circle = circle;
        this.circleBack = circleUserBack;
        if (circleX==0){
            circleX = (int) circle.getX();
            circleOff = circle.getWidth()/2;
        }
        circle.animate().x(x - circleOff).setDuration(100).start();
        changeMovieRating(x - circleOff);
    }

    public void changeCircleState(MotionEvent event){
        float eventX = event.getX() - circleOff;
//        Log.d("happy", "changeCircleState: "+ Float.toString(eventX));
        if (eventX>=0 && eventX<= totalWidth) {
            circle.setX(eventX);
//            Log.d("happy", "changeCircleState: "+ Float.toString(circle.getX()));
            changeMovieRating((int)circle.getX());
        }
    }
    public float snapBackCircle(){
        float x = circle.getX();
//        Log.d("happy", "FINISH "+ Float.toString(x));
        circle.animate().x(circleX).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(1000).start();
        circle = null;
        circleBack = null;
        return x;
    }
    public void shrink(View view){
        view.animate().scaleX(0f).scaleY(0f).setInterpolator(new AccelerateInterpolator()).setDuration(500).start();
    }
    public void unShrink(View view){
        view.animate().scaleX(1f).scaleY(1f).setInterpolator(new AccelerateInterpolator()).setDuration(500).start();
    }
    public void restore(View view){
        view.animate().scaleX(1f).scaleY(1f).setInterpolator(new OvershootInterpolator()).setDuration(1300).start();
    }
    public void slideOut(View view){
        view.animate().x(-view.getWidth()).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(500).start();
    }
    public void slideIn(View view){
        view.animate().x(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(500).start();
    }

    public void fadeIn(View view){
        view.setAlpha(0f);
        view.animate().alpha(1f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(500).start();
    }
    public void fadeOut(View view){
        view.setAlpha(1f);
        view.animate().alpha(0f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(500).start();
    }
    public void rotationSlideInFromBottom(View view, int y){
        view.setRotation(-20f);
        view.animate().rotation(0f).y(y).setInterpolator(new OvershootInterpolator()).setDuration(1000).start();
    }
    public Animation rotationSlideOutFromBottom(View view){
        view.setRotation(0f);
        view.animate().rotation(-20f).y(3000).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(1000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (onAnimationEnd!=null) {
                    onAnimationEnd.onComplete();
                    onAnimationEnd = null;
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
        return this;
    }

    public void setLayoutTransitionAnimation(LinearLayout layout){
        Animator scaleDown = ObjectAnimator.ofPropertyValuesHolder((Object)null, PropertyValuesHolder.ofFloat("scaleX", 1, 0), PropertyValuesHolder.ofFloat("scaleY", 1, 0));
        scaleDown.setDuration(1000);
        scaleDown.setInterpolator(new OvershootInterpolator());

        Animator scaleUp = ObjectAnimator.ofPropertyValuesHolder((Object)null, PropertyValuesHolder.ofFloat("scaleX", 0, 1), PropertyValuesHolder.ofFloat("scaleY", 0, 1));
        scaleUp.setDuration(1000);
        scaleUp.setStartDelay(1000);
        scaleUp.setInterpolator(new OvershootInterpolator());

        LayoutTransition itemLayoutTransition = new LayoutTransition();
        itemLayoutTransition.setAnimator(LayoutTransition.APPEARING, scaleUp);
        itemLayoutTransition.setAnimator(LayoutTransition.DISAPPEARING, scaleDown);

        layout.setLayoutTransition(itemLayoutTransition);
    }
}
