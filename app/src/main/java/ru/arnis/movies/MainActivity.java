package ru.arnis.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.github.nisrulz.sensey.Sensey;
import com.github.nisrulz.sensey.TouchTypeDetector;
import com.yayandroid.parallaxrecyclerview.ParallaxRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.arnis.movies.assistant.Assistant;
import ru.arnis.movies.components.OnCompleteListener;
import ru.arnis.movies.components.RecyclerViewAdapter;
import ru.arnis.movies.model.Movie;
import ru.arnis.movies.model.UserInfo;
import ru.arnis.nav.FloatingButtonBottomNavigation;
import ru.arnis.nav.OnItemClickListener;


public class MainActivity extends AppCompatActivity {
    public static final String TOP250 = "top250";
    public static final String PREMIERES = "premieres";

    private static final String TAG = "happy";

//    @BindView(R.id.settings) View settings;
//    @BindView(R.id.profile_name) TextInputEditText profileName;
    @BindView(R.id.date_tab) TextView date;
//    @BindView(R.id.asistant_chat) LinearLayout asistantChat;
    @BindView(R.id.list) ParallaxRecyclerView recyclerView;
//    @BindView(R.id.reveal) View reveal;
    @BindView(R.id.spin_bars) View spinBars;
    @BindView(R.id.card_overlay) View overlay;
//    @BindView(R.id.assistant_dialog) LinearLayout asistantDialog;
    @BindView(R.id.bottom_nav)
FloatingButtonBottomNavigation bottomNav;

    public static ArrayList<Movie> movies;
    private String savedThread;
    public static boolean touchBroadcast =false;
    private Thread thread;
    private View findView;
    private Animation animation;

    private int ratingMovie;


    private GestureDetector gd;
    private RecyclerViewAdapter adapter;


    private Firebase firebase;
    private DBhelper dBhelper;
    private Assistant asistant;
    private float y=0;
    private boolean canScrollUp;
    private boolean canSwipe = true;
    private boolean once=true;
    private int swipeContoller = 0;

    private UserInfo userInfo;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        date.setText("2016-24-11");
        dBhelper = new DBhelper(this, null, null, 1);

        userInfo = UserInfo.restore(this);
        asistant = new Assistant(this,UserInfo.getUsername());


        firebase = new Firebase(getApplicationContext());
        firebase.db().init(!userInfo.isFirstLaunch());
        firebase.db().setOnMovieLoaded(new OnCompleteListener() {
            @Override
            public void onComplete(Movie movie) {
                adapter.newMovieEntry(movie);
                if (once){
                    date.setText(movie.getDate());
                    animation.revealDate(date);
                    once = false;
                }
            }
        });

        animation = new Animation(recyclerView);
        gd = new GestureDetector(this, new GestureListener());
        movies = new ArrayList<>();
        dBhelper.restoreParams();

        setupRecyclerView();
        setupSensey();
        setUpBottomNav();
    }

    private void setUpBottomNav(){
        bottomNav.init("премьеры","выиграть билеты","тренировать","профиль","о проекте");
        bottomNav.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int index) {
                if (index==2){
                    animation.showPredicting(spinBars,overlay);
                    asistant.train(movies).setOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(Movie movie) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    animation.hidePredicting(spinBars,overlay);
                                }
                            });
                        }
                    });
                } else if (index==1){
                    userInfo.setFirstLaunch(false);
                    setupRecyclerView();
                }
            }
        });
    }

    private void setupRecyclerView(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setItemViewCacheSize(30);
        adapter = new RecyclerViewAdapter(animation,recyclerView.getContext());
        recyclerView.setAdapter(adapter);
//        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int last = 0;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                Log.d("happy", "onScrolled: "+String.valueOf(dy)+" "+String.valueOf(last));
                if (!ViewCompat.canScrollVertically(recyclerView,1))
                    loadMovieBatch();

                canScrollUp = ViewCompat.canScrollVertically(recyclerView, -1);
                if (dy<0&&last>0){
                    bottomNav.showButton();
                    Log.d(TAG, "onScrolled: show");
                } else if (dy>0&&last<=0){
                    bottomNav.hideButton();
                    Log.d(TAG, "onScrolled: hide");
                }

                last = dy;
//                Log.d("happy", Boolean.toString(canScrollUp));
            }
        });

        if (userInfo.isFirstLaunch()){
            setUpRecyclerViewTouchForFirstLaunch();
        } else
            setUpRecyclerViewTouch();
    }
    private void setUpRecyclerViewTouchForFirstLaunch(){
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            int moving=0;
            int maxMoving=40;

            @Override
            public boolean onInterceptTouchEvent(final RecyclerView rv, final MotionEvent e) {
                y = e.getY();
                int action = e.getAction();
                if (action==MotionEvent.ACTION_DOWN){
                    thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                savedThread = Thread.currentThread().getName();
                                Thread.sleep(800);
                                if (moving<maxMoving&&savedThread.equals(Thread.currentThread().getName())){
                                    touchBroadcast =true;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            CardView cardView = (CardView)rv.findChildViewUnder(e.getX(),e.getY());
                                            ratingMovie = rv.getChildAdapterPosition(cardView);
                                            if (cardView!=null)
                                                animation.enterRateState(cardView, (int) e.getX());
                                        }
                                    });
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                } else if (action == MotionEvent.ACTION_UP){
                    savedThread ="";
                    touchBroadcast =false;
                    moving=0;
                    animation.snapRecyclerView();
                } else if (action == MotionEvent.ACTION_MOVE){
                    moving++;
                }
                return touchBroadcast;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                if (findView==null) {
                    findView = animation.getRateCard();
                }

                if (findView!=null)
                    findView.dispatchTouchEvent(e);

                if (e.getAction()==MotionEvent.ACTION_UP){
                    if (ratingMovie!=-1){
                        float rating  = animation.leaveRateState((int)e.getX());
                        movies.get(ratingMovie).setUserRating(rating);
                        Log.d("happy", "movie at "+Integer.toString(ratingMovie)+" received rating of "+ Float.toString(movies.get(ratingMovie).getUserRating()*100));
                    }
                    touchBroadcast =false;
                    findView = null;
                }
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }
    private void setUpRecyclerViewTouch(){
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(final RecyclerView rv, final MotionEvent e) {
                gd.onTouchEvent(e);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });
    }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        Sensey.getInstance().setupDispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }
    private void setupSensey(){
        Sensey.getInstance().init(this);
        TouchTypeDetector.TouchTypListener touchListener = new TouchTypeDetector.TouchTypListener() {
            @Override
            public void onTwoFingerSingleTap() {

            }
            @Override
            public void onThreeFingerSingleTap() {

            }
            @Override
            public void onDoubleTap() {
            }
            @Override
            public void onScroll(int scrollDirection) {
//                if (!canScrollUp && scrollDirection == TouchTypeDetector.SCROLL_DIR_DOWN)
//                    recyclerView.setY(y/15);
//                 else animation.snapRecyclerView();
            }
            @Override
            public void onSingleTap() {

            }

            @Override
            public void onSwipe(int swipeDirection) {
                if (canSwipe&&!bottomNav.isShowing()) {
                    Log.d(TAG, "canswipe");
                    switch (swipeDirection) {
                        case TouchTypeDetector.SWIPE_DIR_LEFT:
                            swipeLeft();
                            canSwipe = false;
                            once = true;
                            break;
                        case TouchTypeDetector.SWIPE_DIR_RIGHT:
                            swipeRight();
                            canSwipe = false;
                            once = true;
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
            }

            @Override
            public void onLongPress() {

            }
        };
        Sensey.getInstance().startTouchTypeDetection(touchListener);
    }
    private void swipeRight() {
        if (swipeContoller>-3){
            swipeContoller--;
            animation.exitRight().setOnAnimationEnd(new ru.nn2.components.OnCompleteListener() {
                @Override
                public void onComplete() {
                    adapter.reset();
                    recyclerView.removeAllViews();
                    loadMovieBatch();
                    canSwipe = true;
                }
            });
            firebase.db().reset();
            firebase.db().setMoviesToLoad(PREMIERES,swipeContoller);
        }

    }
    private void swipeLeft() {
        if (swipeContoller<3){
            swipeContoller++;
            animation.exitLeft().setOnAnimationEnd(new ru.nn2.components.OnCompleteListener() {
                @Override
                public void onComplete() {
                    adapter.reset();
                    recyclerView.removeAllViews();
                    loadMovieBatch();
                    canSwipe = true;
                }
            });
            firebase.db().reset();
            firebase.db().setMoviesToLoad(PREMIERES,swipeContoller);
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        dBhelper.backUpParams();
        movies.clear();
    }


//    @OnClick(R.id.applu_profile) void applyProfile(){
//        String input = profileName.getText().toString();
//        if (input.contains("стереть")){
//            String del = input.split(" ")[1];
//            if (profiles.contains(del)){
//                Toast.makeText(this, "стираем " + del, Toast.LENGTH_LONG).show();
//                asistant.reset();
//                profiles.remove(del);
//                finish();
//            }
//
//            if (del.equals("все")){
//                dBhelper.clearTables();
//                Parameter.reset();
//                profiles.clear();
//                finish();
//            }
//        } else {
//            if (!profiles.contains(input))
//                profiles.add(input);
//
//            asistant = new Assistant(getApplicationContext(),input);
//        }
//    }


    @Override
    public void onBackPressed() {
        if (bottomNav.isShowing()){
            bottomNav.forceHide();
        }
//        else if (asistant.getMode()==Context.BROWSE_RATE){
//            adapter.reset();
//            movies.clear();
//            firebase.db().reset();
//            asistant.setContext(new Context.Start());
//        }
        else
            super.onBackPressed();
    }


//    private void generateDialogs() {
//        if (!asistant.isActive()) {
//            showAsistant();
//            generateReplies(
//                    new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    canSwipe = false;
//                    recyclerView.removeAllViews();
//                    adapter.reset();
//                    firebase.db().setMoviesToLoad(TOP250,0);
//                    loadMovieBatch();
//                    hideAssistant();
//                }
//            } , new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    animation.showPredicting(spinBars,overlay);
//                    asistant.train(movies).setOnCompleteListener(new OnCompleteListener() {
//                        @Override
//                        public void onComplete(Movie movie) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    animation.hidePredicting(spinBars,overlay);
//                                    asistant.nextMode();
//                                }
//                            });
//                        }
//                    });
//                    hideAssistant();
//
//                }});
//        } else hideAssistant();
//
//    }


    private void loadMovieBatch(){
        firebase.db().load(5);
    }

    public class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            int pos = recyclerView.getChildAdapterPosition(recyclerView.findChildViewUnder(e.getX(),e.getY()));
            if (pos!=-1) {
                asistant.predictMovie(movies.get(pos));
                Intent intent = new Intent(getApplicationContext(), MoviePage.class);
                intent.putExtra("pos", pos);
                startActivity(intent);
            }
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            final CardView card = (CardView)recyclerView.findChildViewUnder(e.getX(),e.getY());
            int pos = recyclerView.getChildAdapterPosition(card);
            if (pos!=-1)
                asistant.predictMovie(movies.get(pos)).setOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(Movie movie) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                animation.setRatingTextColor(card,asistant.getResult(),true).setOnAnimationEnd(new ru.nn2.components.OnCompleteListener() {
                                    @Override
                                    public void onComplete() {
//                                        asistantChat.removeAllViews();
                                    }
                                });
//                                asistant.reply();
                            }
                        });

                    }

//                    @Override
//                    public void onResultReady(final double[] result) {
//
//                    }
                });
            return false;
        }
    }
}




//    private void generateHelp(){
//        Button help = new Button(this);
//        help.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        help.setText("Помошь");
//        help.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                asistant.askHelp();
//            }
//        });
//        asistantDialog.addView(help);
//    }
//    private void generateReplies(final View.OnClickListener... listeners){
//        for (String dialog:asistant.getDialogTitles()) {
//            Button button = new Button(this);
//            button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//            button.setText(dialog);
//            // TODO: 03/12/2016 works with just one
//            button.setOnClickListener(listeners[0]);
//            asistantDialog.addView(button);
//        }
//
//        generateHelp();
//    }
//    private void showAsistant(){
//        asistant.activate();
//        animation.showAsistantReveal(null,asistantDialog);
////        ((Animatable)reveal.getBackground()).start();
////        asistant.invoke();
//    }
//    private void hideAssistant(){
//        asistant.activate();
//        animation.hideAsistantReveal(null,asistantDialog);
//        asistantDialog.removeAllViews();
////        asistant.getBM().clearBubbles();
//    }
