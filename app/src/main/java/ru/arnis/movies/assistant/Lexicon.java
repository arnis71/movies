package ru.arnis.movies.assistant;

import java.util.Random;

/**
 * Created by arnis on 16/11/2016.
 */

public class Lexicon {
    private Random rnd;
    private int schRatingHelp;

    public Lexicon() {
        rnd = new Random();
    }

    public String start(Context context) {
        if (context instanceof Context.BrowseRate)
            return ratingWelcome();
        else if (context instanceof Context.Validation)
            return validWelcome();

        return null;
    }

    protected String reply(Context context, int index, int value) {
        if (context instanceof Context.BrowseRate)
            return ratingReply(index, value);
        else if (context instanceof Context.Validation)
            return validReply(index);

        return null;
    }

    protected String help(Context context) {
        if (context instanceof Context.BrowseRate)
            return ratingHelp();

        return null;
    }

    protected String comment(Context context, int value) {
        if (context instanceof Context.BrowseRate)
            return ratingComment(value);

        return null;
    }

    protected String finish(Context context) {
        if (context instanceof Context.Validation)
            return validFIN();

        return null;
    }


    private String ratingWelcome() {
        return ratingWECLOME[0];
    }

    private String ratingReply(int rating, int confidence) {
        return ratingREPLY[rating][confidence];
    }

    private String ratingHelp() {
        if (schRatingHelp < ratingHELP.length)
            return ratingHELP[schRatingHelp++];
        else return ratingHELP[schRatingHelp=0];
    }

    private String ratingComment(int confidence) {
        return ratingCOMMENTS[confidence];
    }

    private String validWelcome() {
        return validWELCOME[0];
    }

    private String validReply(int index) {
        return validREPLY[index][rnd.nextInt(validREPLY[0].length)];
    }

    private String validFIN() {
        return validFIN[rnd.nextInt(validFIN.length)];
    }



    private final String[] ratingWECLOME = new String[]{
            "Привет, чем я могу помочь?"
    };
    private final String[] ratingHELP = new String[]{
            "Отметь те фильмы которые тебя нравятся из этого списка",
            "Нажми на табличку с фильмом и держи палец пока не появиться полоска",
            "Чтобы поставить рейтинг подвинь слайдер в нужное место",
            "Влево если фильм тебе не понравился и вправо если понравился",
            "Удобно правда? :)"
    };
    private final String[][] ratingREPLY = new String[][]{
            {
                    "Мне кажется, что ты не оценишь этот фильм",
                    "Я думаю, что это плохой фильм для тебя",
                    "Лучше не смотри этот фильм, тебе он не понравиться"
            },
            {
                    "Нуууу...",
                    "Ты можешь найти фильмы которые тебе понравяться больше чем этот",
                    "Не самый лучший вариант иходя из твоих предпочтений"
            },
            {
                    "Подбрось монетку",
                    "Есть шанс что что-то в этом фильме тебе может понравиться",
                    "Можешь дать этому фильму шанс"
            },
            {
                    "Показания смешанные но скорее всего тебе понравиться этот фильм",
                    "Если есть возможность, посмотри этот фильм",
                    "Это хороший фильм, посмотри его"
            },
            {
                    "Если я угадал, то ты офигеешь от этого фильма",
                    "По-моему это тот самй фильм!",
                    "Тебе очень понравиться этот фильм, посмотри его"
            }
    };
    private final String[] ratingCOMMENTS = new String[]{
            "Мне с трудом удается понять твои предпочтения",
            "Работаю",
            "Твои вкусы легко проанализировать"
    };
    // NO FIN

    private final String[] validWELCOME = new String[]{
            "Как бы ты оценил этот фильм?"
    };
    private final String[][] validREPLY = new String[][]{
            {
                    "Ура, я угадал!",
                    "Дааааа",
                    "Вот я молодец)"
            },
            {
                    "Ну вот, а я думал я знаю твои вкусы",
                    "Эххххх, промах",
                    "Ну чтож, все ошибаются",
            }
    };
    // NO HELP
    // NO COMMENT
    private final String[] validFIN = new String[]{
            "Очень странно что ты поставил такую оценку",
            "Я не ожидал такого ответа",
            "Приму к сведению"
    };
}
