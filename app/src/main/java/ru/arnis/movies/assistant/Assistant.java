package ru.arnis.movies.assistant;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.arnis.movies.MovieDataConverter;
import ru.arnis.movies.components.OnCompleteListener;
import ru.arnis.movies.model.Movie;
import ru.arnis.movies.model.Parameter;
import ru.nn2.net.ActivationFunction;
import ru.nn2.net.NetDimen;
import ru.nn2.net.NeuronNet;
import ru.nn2.components.TrainingSet;

/**
 * Created by arnis on 16/11/2016.
 */

public class Assistant {
    private NeuronNet neuronNet;
//    private NeuronNet paramNet;
    private ExecutorService simpleTask;
//    private ExecutorService complexTask;
//    private AsistantBubbleManager bubbleManager;
    private Context appContext;
    private OnCompleteListener onComplete;
//    private ru.arnis.movies.assistant.Context context;
    private boolean active = false;
//    private int[] replies;
    private String username;
    private MovieDataConverter movieConverter;

    private static String TAG = "assistant";
    public void setOnCompleteListener(OnCompleteListener onComplete) {
        this.onComplete = onComplete;
    }
    public boolean isActive() {
        return active;
    }
    public void activate() {
        if (active)
            active = false;
        else active = true;
    }

    public Assistant(Context appContext, String username) {
        this.username = username;
        movieConverter = new MovieDataConverter(appContext);
//        replies = new int[2];
        this.appContext = appContext;
        simpleTask = Executors.newFixedThreadPool(2);
//        complexTask = Executors.newFixedThreadPool(6);
        simpleTask.execute(new Runnable() {
            @Override
            public void run() {
                createNN(new NetDimen(MovieDataConverter.INPUT_FIELDS,MovieDataConverter.OUTPUT_FIELDS,new int[]{9}));
            }
        });
//        context = ru.arnis.movies.assistant.Context.init();
    }

//    public void setBubbleManager(AsistantBubbleManager bubbleManager) {
//        this.bubbleManager = bubbleManager;
//    }
//    public AsistantBubbleManager getBM(){
//        return bubbleManager;
//    }
//    public void nextMode(){
//        context = ru.arnis.movies.assistant.Context.next(context);
//    }
//    public String[] getDialogTitles(){
//        String[] dialogs;
//        if (context.getMode() == ru.arnis.movies.assistant.Context.BROWSE_RATE){
//            dialogs = new String[]{"тренировать"};
//        } else {
//            dialogs = new String[]{"тренировать"};
//        }
//        return dialogs;
//    }

//    public void invoke(){
//        String text = context.welcome();
//        bubbleManager.newChatBubble(false).setText(text);
//    }
//    public void askHelp(){
//        String text = context.help();
//        bubbleManager.newChatBubble(false).setText(text);
//    }
//    public void reply(){
//        String text = context.reply(replies[0],replies[1]);
//        bubbleManager.newChatBubble(false).setText(text);
//    }

    private void createNN(final NetDimen dimensions){
            simpleTask.execute(new Runnable() {
                @Override
                public void run() {
                    NeuronNet net = new NeuronNet.Builder(NeuronNet.getNN(NeuronNet.FEEDFORWARD_NN))
                            .setDimensions(dimensions)
                            .setBias(true)
                            .setErrorCalculation(NeuronNet.MSE)
                            .setActivationFunction(ActivationFunction.SIGMOID)
                            .addBrains(appContext)
                            .build();

                    net.setName(username);
                    net.loadBrains();
                    net.forceStopAtError(0.1);
                    net.setMaxIterations(10000);

                    neuronNet = net;
//                    paramNet = net;
                }
            });
    }
    private void createTrainingSet(ArrayList<Movie> movies) {
        TrainingSet trainingSet = new TrainingSet();
//        trainingSet.addEntry(new TrainingSet.Set(Parameter.createParamVector(),new double[]{1}));
//        paramNet.setTrainingSet(trainingSet);
        for (Movie movie:movies){
            if (movie.getUserRating()!=0) {
                trainingSet.addEntry(new TrainingSet.Set(movieConverter.convertMovieData(movie),movieConverter.convertMovieRating(movie)));
            }
        }
        neuronNet.setTrainingSet(trainingSet);
    }
    private void createWorkSet(Movie movie){
//        paramNet.setTrainingSet(new TrainingSet().addEntry(new TrainingSet.Set(Parameter.createSatisfactionVector(movie))));
        neuronNet.setTrainingSet(new TrainingSet().addEntry(new TrainingSet.Set(movieConverter.convertMovieData(movie))));
    }
    public Assistant train(final ArrayList<Movie> movies){
        simpleTask.execute(new Runnable() {
            @Override
            public void run() {
                createTrainingSet(movies);

                //paramNet
                neuronNet.setMode(NeuronNet.TRAINING_MODE);
                neuronNet.start();
                neuronNet.join();
                neuronNet.store(appContext);

                onComplete.onComplete(null);
                Log.d("happy", "done");
            }
        });
        return this;
    }
    public Assistant predictMovie(final Movie movie){
        simpleTask.execute(new Runnable() {
            @Override
            public void run() {
                createWorkSet(movie);

                //paramNet
                neuronNet.setMode(NeuronNet.WORKING_MODE);
                neuronNet.startWithListener(new ru.nn2.components.OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            final double[] result = neuronNet.getOutput();
                            movie.setNnRating(result);
                            Parameter.createSatisfactionVector(movie);
                            if (onComplete!=null)
                                onComplete.onComplete(null);
                        }
                    });
            }
        });
        return this;
    }

    public double[] getResult(){
        return neuronNet.getOutput();
//        return paramNet.getOutput();
    }

    public int[] analyseResult(double[] result) {
        double max = 0;
        int index=0;
        int value = 0;
        for (int i = 0;i<result.length;i++) {
            if (result[i] > max){
                max = result[i];
                index = i;
            }
        }
        if (max<.3)
            value = 0;
        else if (max<.6)
            value = 1;
        else if (max<1.2)
            value = 2;

        return new int[]{index,value};
    }
    public void reset(){
        neuronNet.resetBrains(appContext);
    }

    public static int checkPredcitionSuccess(Movie movie){
        int user = (int) (movie.getUserRating()*100);
        double[] nn = movie.getNnRating();
        int pos = 0;
        double max = 0;
        for (int i=0;i<nn.length;i++)
            if (nn[i]>max){
                max = nn[i];
                pos=i;
            }

        int userAdj = user/20;

        int difference = userAdj-pos;

        return difference;
    }
}
