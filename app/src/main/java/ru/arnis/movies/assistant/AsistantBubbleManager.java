package ru.arnis.movies.assistant;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ru.arnis.movies.R;

/**
 * Created by arnis on 23/11/2016.
 */

public class AsistantBubbleManager {
    private static final int MAX_BUBBLES = 3;

    private LinearLayout chatBubbles;
    private android.content.Context context;
    private ArrayList<View> bubbles;

    public AsistantBubbleManager(android.content.Context context, LinearLayout chatBubbles) {
        this.context = context;
        this.chatBubbles = chatBubbles;
        bubbles = new ArrayList<>(MAX_BUBBLES);
    }

    protected TextView newChatBubble(boolean clear){
        View bubble = LayoutInflater.from(context).inflate(R.layout.bubble_chat,null);
        bubbles.add(bubble);
        if (clear||bubbles.size()>=MAX_BUBBLES){
            chatBubbles.removeAllViews();
            bubbles.clear();
        }

        chatBubbles.addView(bubble);
        Log.d("happy", Integer.toString(chatBubbles.getChildCount()));
        return (TextView) bubble.findViewById(R.id.dialog_text);
    }

    public void clearBubbles(){
        if (bubbles!=null && chatBubbles!=null){
            bubbles.clear();
            chatBubbles.removeAllViews();
        }
    }


}
