package ru.arnis.movies.assistant;

import ru.arnis.movies.MainActivity;

/**
 * Created by arnis on 16/11/2016.
 */

public abstract class Context {
    static final int BROWSE_RATE = 0;
    static final int VALIDATION = 1;

    private Lexicon lexicon;

    private Context() {
        this.lexicon = new Lexicon();
    }
    static Context init(){
        return new BrowseRate();
    }
    static Context next(Context context){
        if (context instanceof BrowseRate)
            return new Validation();
        else return new BrowseRate();
    }


    String welcome(){
        return lexicon.start(this);
    }
    String reply(int index, int value){
        return lexicon.reply(this,index,value);
    }
    String help(){
        return lexicon.help(this);
    }
    String comment(int value){
        return lexicon.comment(this,value);
    }
    String finish(){
        return lexicon.finish(this);
    }
    int getMode(){
        if (this instanceof BrowseRate)
            return BROWSE_RATE;
        else return VALIDATION;
    }

    public static class BrowseRate extends Context {
        private String in;

        public BrowseRate() {
            super();
            in = MainActivity.PREMIERES;
        }
    }
    public static class Validation extends Context {

    }
}
