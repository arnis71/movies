package ru.arnis.movies.components;

import ru.arnis.movies.model.Movie;

/**
 * Created by arnis on 24/09/2016.
 */

public interface OnCompleteListener {
    void onComplete(Movie movie);
//    void onResultReady(double[] result);
}
