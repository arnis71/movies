package ru.arnis.movies.components;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yayandroid.parallaxrecyclerview.ParallaxImageView;
import com.yayandroid.parallaxrecyclerview.ParallaxViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.arnis.movies.Animation;
import ru.arnis.movies.MainActivity;
import ru.arnis.movies.model.Movie;
import ru.arnis.movies.R;

import static ru.arnis.movies.MainActivity.movies;

/**
 * Created by arnis on 10/11/2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private Animation animation;

    public RecyclerViewAdapter(Animation animation, Context context) {
        this.context = context;
        this.animation = animation;
    }

    public void newMovieEntry(Movie movie) {
        movies.add(movie);
        notifyItemInserted(movies.size()-1);
    }

    public void reset(){
        movies.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder extends ParallaxViewHolder {
        @BindView(R.id.main_poster)
        ParallaxImageView poster;
        @BindView(R.id.background)
        View background;
        @BindView(R.id.movie_country)
        TextView country;
        @BindView(R.id.movie_genre)
        TextView genre;
        @BindView(R.id.main_rus_title)
        TextView titleRus;
        @BindView(R.id.main_eng_title)
        TextView titleEng;

        @BindView(R.id.stride)
        ImageView slider;

        @Override
        public int getParallaxImageId() {
            return R.id.main_poster;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final CardView card = (CardView) LayoutInflater.from(context).inflate(R.layout.card_tile, parent, false);

        card.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MainActivity.touchBroadcast) {
                    int action = motionEvent.getAction();
                    if (action == MotionEvent.ACTION_MOVE) {
                        float x = motionEvent.getX();
                        animation.changeMovieRating((int) x);
                    }
                }

                return false;

            }
        });
//        card.animate().y(200).setDuration(1000);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {


        Movie currentMovie = movies.get(position);
        Glide.with(context).load(currentMovie.getPosterURL()).into(holder.poster);
//        Glide.with(context).load(currentMovie.getPosterURL())
////                .listener(GlidePalette.with(currentMovie.getPosterURL())
////                        .use(GlidePalette.Profile.VIBRANT)
////                        .intoBackground(holder.background)
////
////        )
//                .into(holder.poster);

        holder.titleRus.setText(currentMovie.getTitleRUS());
        holder.titleEng.setText(currentMovie.getTitleENG());
        holder.genre.setText(currentMovie.getArrAsString(currentMovie.getGenre()));
        holder.country.setText(currentMovie.getArrAsString(currentMovie.getCountries()));
//        String info = currentMovie.getKeywordsAsString();// + "\n" + currentMovie.getGenreAsString() + "\n" + currentMovie.getMpaa() + "\n" + currentMovie.getDuration();
//        holder.info.setText(info);

        holder.getBackgroundImage().reuse();

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

}
